\documentclass[12pt]{article}
\usepackage{color,fullpage,natbib}
\usepackage{hyperref}
\hypersetup{pdftitle={Response to comments on "Multiporosity flow in fractured low-permeability rocks: extension to shale reservoirs" 2014WR016502},pdfauthor={Kristopher L. Kuhlman, Bwalya Malama, and Jason E. Heath},pdfborder={0 0 0},colorlinks=false,linkcolor=blue,urlcolor=blue}

\title{Response to comments on ``Multiporosity flow in fractured low-permeability rocks: extension to shale reservoirs'' \texttt{2014WR016502} }
\author{Kristopher L. Kuhlman, Bwalya Malama, and Jason E. Heath}
\begin{document}
\maketitle

The authors want to thank the reviewers and editors for their insightful and timely comments. This document gives our responses to individual comments. We have removed most of the application to shale reservoirs and implemented most of the comments in the revised manuscript.  Reviewers' comments are smaller italic font; our responses follow in regular font.

\section{Associate Editor}
\begin{quote}{\scriptsize \textit{I have only one significant concern. Notice this same concern was raised by reviewers 1 and 2, and more, it was (in my personal opinion, do not take it for granted) why reviewer \#1 ticked the "major revision" box. The problem is the overselling of the work to be potentially applied to shale formations. The word "shale" is written in the title, and it is the aim of the introduction, mostly devoted to flow in shales due to fracking practices. But, on the other hand, the work presented is applicable in general and there is no reason for overstressing the applicability to shale formations or to fracking. More, in the abstract there is absolutely no reference to shale rocks. It looks as if the authors had to concur with some funding body, but this is not the case according to the acknowledgement section.}}
\end{quote}

\begin{quote}{\scriptsize \textit{In short, the methodology is not limited to shale reservoirs, but it is rather general. I suggest the authors remove the shale reservoirs from the title and also change the introduction to make it slightly more general in scope. Apart from that, the work is fine and the authors will see that only minor additional comments are raised by the reviewers. Altogether the amount of work involved in producing a new version can be considered minor.}}
\end{quote}

We revised the title and introduction to reduce the focus on hydraulic fracturing and shale systems. We deleted the previous section called ``extensions to shale gas reservoirs'' and removed references to the stimulated reservoir volume (SRV).  The desorption rate has been removed from the derivation, and some of the geometry nomenclature in the derivation was changed ($x_f \rightarrow R$, $y_e \rightarrow L$, and $x_e$ was removed as redundant).  There is a new first paragraph to the introduction that presents a wider introduction and motivation for the multiporosity model. Shale systems are now included only one of several example applications in the introduction.  The revised manuscript is approximately 4 pages shorter than the original manuscript.

\section{Reviewer \#1}

\paragraph{Introduction}
\begin{quote}
{\scriptsize \textit{This paper has an interesting title which addresses a topic of great interest. It is somewhat useful but as written may not be useful for direct application to shale reservoirs for the reasons I have noted below. If, however, all references to shales are removed, then this paper may be considered for publication. The principal difficulty in evaluating a fractured reservoir (even for the WR model) is obtaining the needed data to do justice to that model. But that should not preclude publication of a work like this. Nevertheless, the basic observation I make remains.}}
\end{quote}

\paragraph{Comments}
\begin{enumerate}
\item {\scriptsize \textit{Lines 51-53, page 4: All shale wells that I am aware of are presumed to produce through hydraulic fractures; not merely as a result of the natural fractures in the reservoir rock. There are two distinct potential problems that may be solved: (a) flow in naturally fractured rocks and production from such rocks as considered by Barenblatt et al. and subsequent works, and (b) flow in naturally fractured rocks that incorporate the basic features of the Barenblatt et al. and Kazemi models and presume production through hydraulic fractures (single or multiple); see J. Energy Resour. Technology, 108(2), \url{http://dx.doi.org/10.1115/1.3231251} and SPE Journal, \url{http://dx.doi.org/10.2118/27652-PA}. Of course, there are some who believe that production is the result of a fracture network. If that is the claim (Problem a), then this fact should be explicitly stated; appropriate references cited, and citations given here (Ozkan, Wattenbarger, Blasingame groups etc.) should be removed. This limitation may also be addressed if all references to shales are removed. My objections could also be addressed by considering the problem in Cartesian coordinates (1D) as done by Ozkan et al. That should not be a difficult matter for, based on the solution obtained here, only a minor change in the Ozkan et al. solution would be required. Even then we will only have an approximate solution.}}

The focus of the introduction has been changed (see response to AE comment) and the references to  Ozkan, Wattenbarger, Blasingame, etc. have been removed. 

We have replaced most of the shale and hydraulic fracturing application discussion with a much shorter discussion indicating accurate inclusion of discrete fractures (i.e., those due to hydraulic fracturing) would require representing them explicitly in the model -- using approaches like the analytic element method to represent fractures (page 17, lines 296--306 in revised manuscript).

\item {\scriptsize \textit{To return to the specific observations in Lines 51-53 of page 4, no one really knows anything about the shape of the SRV. The assumptions in Ozkan et al. are merely a convenience to make the problem tractable. Generalizations of their model are available; the lack of hard data is the issue at this time.}}

The discussion about the SRV has been removed in conjunction with the minimization of the focus on shale reservoirs.

\item {\scriptsize \textit{Line 88, Page 6: To my mind, the term multiporosity should only refer to the rock characteristics. The rate term has nothing to do with the characteristics of the rock. The porosity terms appear in the pde defining the system which reflects rock and fluid properties, and the rate term invariably appears in the equation specifying the boundary condition.}}

We agree with the reviewer's opinion that multiporosity only refers to rock characteristics.  There is some confusion about the use of the term ``rate'' in the multirate model of \citet{haggerty1995,haggerty1998}.  In their model, ``rate'' is used in a chemical sense (i.e., ``reaction rate''), and has nothing to do with pumping rate.

We have added a clarification to the end of the introduction (page 6, line 89 of revised manuscript) to minimize confusion.

\item {\scriptsize \textit{Lines 119-121, Page 7: There is a rich literature of analytical solutions pertaining to the KZ approach (J. Petroleum Technology, \url{http://dx.doi.org/10.2118/10780-PA}, Society of Petroleum Engineers Journal, \url{http://dx.doi.org/10.2118/11243-PA}, SPE Formation Evaluation, \url{http://dx.doi.org/10.2118/9902-PA}) to mention just a few papers.}}

We have added the references \citep{raghavan1983new,chen1985pressure,ozkan1987unsteady} as indicated (page 7, lines 108--109 of revised manuscript).

\item {\scriptsize \textit{Line 141, Page 8: There is no way, as of now, to get more parameters. If procedures exist, please provide citations.}}

The ambiguous phrase ``With more porosities, come more free parameters'' was intended to be interpreted in a more philosophical sense (i.e., comparing two different conceptual or numerical models). We have modified this phrase to be more specific and precise (page 7, lines 125--128 of revised manuscript).

\item {\scriptsize \textit{Line 209, Page 11: Gas desorption is a nonlinear problem even if one invokes the pseudopressure transformation. A solution by analytical means is not possible. This section must come out.}}

The section on ``extensions to shale gas reservoirs'' has been removed. Things related to $R_m$ and $R_f$ have been removed from the mathematical derivation and the nomenclature tables.

\item {\scriptsize \textit{Line 229, Page 12: As far as I know, material-balance time is used to let us use the solutions for a constant rate or discharge. Usually people who wish to address fundamental issues, as this paper wishes to do, use Duhamel’s theorem.}}

We agree with the reviewer on this point. This discussion on variable flowrates has been removed (along with the rest of this section), but a shortened discussion on the use of Duhamel's theorem in section 3.1 of revised manuscript remains (page 18, beginning line 318 of revised manuscript).  

\item {\scriptsize \textit{Lines 260-261: The assumption made in Ozkan et al. is different from the one made here. They assume a 1D composite system (Cartesian coordinates). Here the problem is solved in cylindrical coordinates, and the existence of hydraulic fractures is ignored. The Ozkan model works for the reason noted in the paper given in SPE Journal, \url{http://dx.doi.org/10.2118/27652-PA}, and is intended to be used for a limited time.}}

This paragraph has been removed from the revised manuscript.

\end{enumerate}

\newpage
\paragraph{General observations}
\begin{quote}{\scriptsize \textit{Although there is a nice introduction about shale reservoirs, there is very little explanation on the role hydraulic fracturing plays in creating what is usually referred to as the SRV. As one who addresses rather difficult issues in this area on a daily basis, I believe a convincing case for a multiporosity model needs to be made. There is very little discussion on how I would know that a multiporosity model needs to be used. In my experience, none of the results shown here are seen in shale reservoirs.}}
\end{quote}

The revised manuscript no longer stresses the application to hydrofractured shale reservoirs. A short statement at the beginning of the presentation on the radially symmetric solution indicates other solution geometries would be more appropriate to represent discrete fractures in hydrofracked wells (page 17, lines 296--306 of revised manuscript).

%\newpage
\section{Reviewer \#2}
\begin{enumerate}
\item {\scriptsize \textit{The title of the paper and most of the text introduction emphasizes the applicability related to characterization of shale reservoirs. However, this is somewhat is misleading because the methodology is not limited to shale reservoirs. I understand that this is a hot topic at the moment. Still I recommend to remove the shale reservoirs from the title. I will recommend the introduction to discuss further other applications as well.}}

The title and introduction of revised manuscript has significantly reduced the application to shale reservoirs (see AE comment). Additional applications are mentioned now in the first paragraph of the introduction.

\item {\scriptsize \textit{Please add discussion related to the so-called multi-permeability conceptualizations. In the area of multi-continua conceptualization, the multi-permeability might be better suited than the multi-porosity models. I also understand the difficulties to develop analytical (or semi- analytical) solutions for the case of "multi-permeability" flow. However, it is important to contrast the differences between multi-permeability and multi-porosity conceptualizations.}}

The alternative multi-permeability conceptualization has been expanded (page 5, lines 71--80 of revised manuscript).

\item {\scriptsize \textit{Typically the storativity (storage capacity) of the geologic formations is predominantly controlled by compressibility of the geologic materials rather than the fluids. It is emphasized that the methodology is applicable for slightly compressible fluids. Please discuss how this relates to the compressibility of the formation.}}

The compressibility of the formation is included in the $c_m$ and $c_f$ terms, in a format commonly used in the petroleum industry. The formulation does involve porosity, but is not only limited to the fluid-filled portion of the domain. From a hydrogeology point of view the $c_{\ell}\phi_{\ell}$ product can be considered equivalent to confined storage (which can alternatively be defined as a sum of matrix and fluid compressibility).

Typically, only the dimensionless parameters are estimated from data (e.g., the dimensionless storage ratio $\omega_f$ or $\omega_j$), and the interpretation of these in terms of more physically fundamental quantities is up to the analyst.  This alternative conceptualization is now mentioned (page 10, line 186--189 of revised manuscript).

\item {\scriptsize \textit{The methodology does not consider "flow in the matrix beyond the radial extent of the fracture" and assumes that the flow in the matrix only occurs perpendicularly to the fracture plane. This is a reasonable assumption. However there will be edge effects along the fracture boundary that are currently ignored. As a results, the proposed solution may not suitable in the small domain (small domain radius) because the edge effects might have huge impacts. Can you provide guidance regardless the scale of the applicable problems?}}

The flow domain can be made as large as needed (possibly infinite), and the boundary type at the radial extent of the domain can be constant pressure (Type I), no-flow (Type II), or a combination of these two (Type III). 

The appropriate choice of model domain size is up to the analyst. Our presentation of analytical solutions with multiple alternative boundary conditions is more general than most, which commonly only have a finite domain with a no-flow boundary or an infinite domain.

\item {\scriptsize \textit{In general, the proposed methodology can be very useful for characterization of the medium properties at the real-world sites. However, flow-regime complexities, heterogeneities, and measurement errors (uncertainties) may have major impact on the obtained pressure results. Also the presented pressure-time curve smay suggest relatively low sensitivity of the observed pressures to model parameters. So in reality, it might be challenging to estimate what is the ratio of the matrix / fracture permeability or the number of multi-porosity continua. Can you provide analysis and discussion based on a formal sensitivity analyses on the applicability of proposed methodology to characterize the properties of the geologic media?}}

A formal sensitivity analysis for typical sets of dimensionless parameters would be a very interesting test of this (or any numerical model for flow), but is beyond the scope of this manuscript.

\end{enumerate}

\section{Reviewer \#3}

\begin{enumerate}
\item {\scriptsize \textit{The name ``Barenblatt'' is spelt thus in English. (line 510 and elsewhere) \texttt{http://en.wikipedia.org/wiki/Grigory\_Barenblatt}}}

``Barenblat'' $\rightarrow$ ``Barenblatt'' throughout

\item {\scriptsize \textit{Similarly, it is ``de Hoog'' (lower case) and J.H. Knight and A.N. Stokes (line 533 and elsewhere)}}

``De Hoog'' $\rightarrow$ ``de Hoog'' throughout

\item {\scriptsize \textit{On line 519, ``systemsa'' is incorrect.}}

fixed typo

\item {\scriptsize \textit{On line 599, presumably the propernames should have initial capitals.}}

proper place names have been capitalized in this reference

\item {\scriptsize \textit{line 632, ``natural''.}}

fixed typo

\item {\scriptsize \textit{line 633, I presume that the journal didn't call itself ``Old SPE Journal''. It is usually cited as (something like): Warren, J.E., and P.J. Root. 1963. The behavior of naturally fractured reservoirs. Society of Petroleum Engineers 3: 245-255.}}

``Old SPE Journal'' $\rightarrow$ ``Society of Petroleum Engineers Journal'' throughout

\end{enumerate}

\bibliographystyle{agufull08}
\bibliography{dual-porosity}


\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
