from fipy import CylindricalGrid2D, CellVariable, FaceVariable, TransientTerm, DiffusionTerm, VanLeerConvectionTerm, Matplotlib2DGridContourViewer
import numpy as np
import matplotlib.pyplot as plt

Lx = 100.0
nx = 100
dx = Lx/nx

h = 20.0
dy = 0.25
ny = int(h/dy)
frac = 5*dy

mu = 1.0E-3 # viscosity

# fracture properties
phif = 0.001   # porosity
kf = 1.0E-1   # permeability
cf = 1.0E-2   # capacity
Df = 1.0E-1   # diffusion coeff
Rf = 1.0      # retardation coefficient

# matrix properties
phim = 0.099   # porosity
km = 1.0E-5   # permeability
cm = 1.0E-0   # capacity
Dm = 1.0E-1   # diffusion coeff
Rm = 1.0      # retardation coefficient

m = CylindricalGrid2D(dx=dx, nx=nx, dy=dy, ny=ny, origin=[[1.0],[0.0]])
x,y = m.faceCenters


# fracture definitions
infrac = (y <= frac) & (x <= Lx*0.75)
fracbdry = (y <= frac) 

# change in pressure in fractures and matrix
p = CellVariable(name='relative pressure change $\\Delta\\psi$', mesh=m, value=0.0)

# setup flow properties for mesh & fracture
alpha = FaceVariable(mesh=m, value=km/(phim*mu*cm))
alpha.setValue(kf/(phif*mu*cf), where=infrac)

k = FaceVariable(mesh=m, value=km)
k.setValue(kf, where=infrac)

phi = FaceVariable(mesh=m, value=phim)
phi.setValue(phif, where=infrac)

D = FaceVariable(mesh=m, value=Dm)
D.setValue(Df, where=infrac)

R = FaceVariable(mesh=m, value=Rm)
R.setValue(Rf, where=infrac)

# specified flux in fracture at one end/ no-flow at other
specFlux = -1.0E-4
#p.faceGrad.constrain([[-specFlux],[0.0]], m.facesLeft & fracbdry)
p.constrain(-1.0, m.facesLeft & fracbdry)

## non-zero initial concentration section of domain
#c0 = np.zeros(nx, 'd')
##c0[20:50] = 1.0
#c0 = np.linspace(0.0,1.0,nx)
#c0 = np.ones(nx, 'd')

## concentration in fractures and matrix
c = CellVariable(name="relative concentration $c$", mesh=m, value=1.0)
X,Y = m.cellCenters 
c.setValue(0.0, where=(Y <= frac) & (X <= Lx*0.75))

## flux out of domain on left in fracture
# Robin boundary condition with c0 = 0 in the borehole
c.faceGrad.constrain(c.faceValue*p.faceGrad*k/(R*phi), m.facesLeft & fracbdry)
#c.constrain(0.0, m.facesBottom)

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# flow equation
Peq = TransientTerm(1.0,var=p) == DiffusionTerm(alpha,var=p)

# transport equation
Ceq = (TransientTerm(1.0,var=c) == DiffusionTerm(D,var=c) - 
       VanLeerConvectionTerm(-p.faceGrad*k/(R*phi),var=c))

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fig = plt.figure(1,figsize=(11,7))
axp = fig.add_subplot(211)
axc = fig.add_subplot(212)

Hviewer = Matplotlib2DGridContourViewer(vars=p,title='Head',
                                        datamin=-1.0,datamax=0.0,axes=axp)
Hviewer.plot()

Cviewer = Matplotlib2DGridContourViewer(vars=c,title='Concentration',
                                        datamin=0.0,datamax=1.0,axes=axc)
Cviewer.plot()

r = plt.suptitle('t=0.0')

time = 0.0

for j,timeStep in enumerate(np.logspace(-2,3,100)):
    time += timeStep
    print j,time

    Peq.solve(dt=timeStep)
    Hviewer.plot()

    Ceq.solve(dt=timeStep)
    Cviewer.plot()

    r.set_text('t=%.2g' % time)
    ##plt.savefig('fracture-flow-and-transport-%4.4i.png' % j)
    np.savez('fracture-flow-and-transport-%4.4i.npz' % j, p=p,c=c)

# this command will merge pngs into a windows-playable movie for ppt
## ffmpeg -f image2 -r 1/0.25 -i fracture-flow-and-transport-%04d.png -r 25 -sameq video.avi
