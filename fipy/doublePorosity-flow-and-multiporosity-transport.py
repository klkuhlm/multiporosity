from fipy import CylindricalGrid1D, CellVariable, FaceVariable, TransientTerm, DiffusionTerm, ExponentialConvectionTerm, ImplicitSourceTerm, Viewer
#import matplotlib
#matplotlib.use('Agg')
#import matplotlib.pyplot as plt
import numpy as np

L = 50.0
nx = 200
dx = L/nx
#-#outlocs = [0,49,69]

rt2 = np.sqrt(2.0)
rt2pi = np.sqrt(2.0*np.pi)

phif = 0.01   # fracture porosity
phim = 0.09  # matrix porosity

gamma = 0.5   # fracture-matrix head transfer rate
#beta = [0.2,0.2,0.3]  # fracture-matrix conc transfer rate
mu = 1.0
sigma = 2.0
print 'mode of log(p(omega)):',mu - sigma**2
logom = [-3.0, -1.5, 0.0, 1.0, 2.0, 3.0] # manually picked abcissa to sample for finite sum
omega = [np.exp(z) for z in logom]  

# width of intervals for finite sum
deltaOmega = [np.exp(-2.5) - np.exp(-4.0),
              np.exp(-0.75) - np.exp(-2.5),
              np.exp(0.5) - np.exp(-0.75),
              np.exp(1.5) - np.exp(0.5),
              np.exp(2.5) - np.exp(1.5),
              np.exp(4.0) - np.exp(2.5)] 

D = 1.0E-1   # fracture diffusion coeff
Rf = 1.0     # fracture retardation coefficient
Rm = 1.0     # matrix retardation coefficient

betaT = 1.0  #phim*Rm/(phif*Rf)
DR = D/Rf

Kf = 1.0       # fracture permeability
Ssf = 5.0E-2   # fracture specific storage

m = CylindricalGrid1D(dx=dx, nx=nx, origin=(0.1))

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# pseudo steady-state dual porosity flow solution (Warren & Root)
# analogous dual porosity transport solution 

# head/drawdown in fractures and matrix
pf = CellVariable(name='$p_f$', mesh=m, value=0.0)
pm = CellVariable(name='$p_m$', mesh=m, value=0.0)

# specified flux at one end/ constant head at other
specFlux = 1.0E0
pf.faceGrad.constrain([specFlux,], m.facesLeft)
pf.faceGrad.constrain([0.0,], m.facesRight)

# no flow condition for matrix at both ends
pm.constrain(0.0, m.facesLeft)  # <= faceGrad.
pm.faceGrad.constrain([0.0,], m.facesRight)

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# concentration in fractures and matrix

## non-zero initial concentration section of domain
#c0 = np.zeros(nx, 'd')
##c0[20:50] = 1.0
c0 = np.linspace(0.0,1.0,nx)
#c0 = np.ones(nx, 'd')

cf = CellVariable(name="$c_f$", mesh=m, value=c0)  # fresh water in fractures?

#cf.constrain(0.0, m.facesLeft)
cf.faceGrad.constrain([specFlux], m.facesLeft)
#cf.constrain(0.0, m.facesLeft)
cf.faceGrad.constrain([0.0,], m.facesRight)

cm = [CellVariable(name="$c_{m1}$", mesh=m, value=1.0),
      CellVariable(name="$c_{m2}$", mesh=m, value=1.0),
      CellVariable(name="$c_{m3}$", mesh=m, value=1.0),
      CellVariable(name="$c_{m4}$", mesh=m, value=1.0),
      CellVariable(name="$c_{m5}$", mesh=m, value=1.0),
      CellVariable(name="$c_{m6}$", mesh=m, value=1.0)]

for cmv in cm:
    cmv.faceGrad.constrain([0.0,], m.facesLeft)
    cmv.faceGrad.constrain([0.0,], m.facesRight)

# lognormal function
lognormal = lambda x : np.exp(-(np.log(x) - mu)**2/(2.0*sigma**2))/(x*sigma*rt2pi)
pOmega = [lognormal(x) for x in omega]

coeff = [a*b*betaT for a,b in zip(deltaOmega,pOmega)]

print 'mu,sigma,betaT',mu,sigma,betaT
print 'omega',omega
print 'deltaOmega',deltaOmega
print 'p(omega)',pOmega
print 'coeff',coeff

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Fracture flow equation
HeqF = TransientTerm(Ssf,var=pf) + TransientTerm(gamma,var=pm) == DiffusionTerm(Kf,var=pf)

# matrix flow equation
HeqM = TransientTerm(1.0,var=pm) == (ImplicitSourceTerm(gamma,var=pf) - 
                                     ImplicitSourceTerm(gamma,var=pm))

# convection diffusion equation for fractures 
# includes sum approximation to infinite integral
CeqF =  (TransientTerm(1.0,var=cf) + TransientTerm(coeff[0],var=cm[0]) + 
         TransientTerm(coeff[1],var=cm[1]) + TransientTerm(coeff[2],var=cm[2]) + 
         TransientTerm(coeff[3],var=cm[3]) + TransientTerm(coeff[4],var=cm[4]) +
         TransientTerm(coeff[5],var=cm[5]) == 
         DiffusionTerm(DR,var=cf) - ExponentialConvectionTerm(-pf.faceGrad,var=cf))

# lumped approach for matrix
CeqM1 = TransientTerm(Rm*phim,var=cm[0]) == (ImplicitSourceTerm(omega[0],var=cf) - 
                                             ImplicitSourceTerm(omega[0],var=cm[0]))

CeqM2 = TransientTerm(Rm*phim,var=cm[1]) == (ImplicitSourceTerm(omega[1],var=cf) - 
                                             ImplicitSourceTerm(omega[1],var=cm[1]))

CeqM3 = TransientTerm(Rm*phim,var=cm[2]) == (ImplicitSourceTerm(omega[2],var=cf) - 
                                             ImplicitSourceTerm(omega[2],var=cm[2]))

CeqM4 = TransientTerm(Rm*phim,var=cm[3]) == (ImplicitSourceTerm(omega[3],var=cf) - 
                                             ImplicitSourceTerm(omega[3],var=cm[3]))

CeqM5 = TransientTerm(Rm*phim,var=cm[4]) == (ImplicitSourceTerm(omega[4],var=cf) - 
                                             ImplicitSourceTerm(omega[4],var=cm[4]))

CeqM6 = TransientTerm(Rm*phim,var=cm[5]) == (ImplicitSourceTerm(omega[5],var=cf) - 
                                             ImplicitSourceTerm(omega[5],var=cm[5]))

# couple pairs of equations
Heqns = HeqF & HeqM 
Ceqns = CeqF & CeqM1 & CeqM2 & CeqM3 & CeqM4 & CeqM5 & CeqM6

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Hviewer = Viewer(vars=(pf,pm),title='Head',legend=4,datamin=-0.3,datamax=0)
###Hviewer = Viewer(vars=(pf,pm),title='Head',axes=hax,legend=4)
Hviewer.plot()
Cviewer = Viewer(vars=(cf,cm[0],cm[1],cm[2],cm[3],cm[4],cm[5]),
                 datamax=1.0,datamin=0.0,title='Concentration',legend=4)
###Cviewer = Viewer(vars=(cf,cm[0],cm[1],cm[2],cm[3],cm[4],cm[5]),datamax=1.0,datamin=-0.25,
###                 title='Concentration',axes=cax,legend=4)
Cviewer.plot()

time = 0.0
split = 3.0E-1
dt = np.concatenate((np.logspace(-4,np.log10(split),20),np.ones((200,))*split),axis=1)

#-#fh = open('solution-double-porosity-flow-and-transport.dat','w')
#-#fmtstr = ','.join('%.5f' for i in range(len(outlocs)*9+1)) + '\n'

##saveFigs = False

###x = m.cellCenters[0] # for plotting
###c = ['g','r','m','c','k','y']

for j,timeStep in enumerate(dt):
    time += timeStep
    print j,time

    ###fig = plt.figure(1,figsize=(11,7))
    ###hax = fig.add_subplot(211)
    ###cax = fig.add_subplot(212)

    Heqns.solve(dt=timeStep)
    ###hax.plot(x,pf.getValue(),'b-',linewidth=2.0,label='fracture')
    ###hax.plot(x,pm.getValue(),'g--',label='matrix')
    ###hax.legend(loc=4)
    ###hax.set_ylim([-4,0])
    ###hax.set_ylabel('downhole pressure')
    ###hax.set_title('pressure change')

    Hviewer.plot()

    #-#out = [time,]
    #-#out.extend([pf.getValue()[i] for i in outlocs])
    #-#out.extend([pm.getValue()[i] for i in outlocs])

    Ceqns.solve(dt=timeStep)
    ###cax.plot(x,cf.getValue(),'b-',linewidth=2.0,label='fracture')
    ###for k in range(6):
    ###    cax.plot(x,cm[k].getValue(),'%s--'%c[k],label='matrix %i'%k)

    ###cax.legend(loc=4)
    ###cax.set_ylim([0,1])
    ###cax.set_ylabel('concentration')
    ###cax.set_xlabel('distance from borehole [m]')
    ###cax.set_title('concentration')
    ###
    ###plt.suptitle('t=%.2g' % time)
    ###plt.savefig('combined-%4.4i.png' % j)
    ###plt.close(1)

    Cviewer.plot()

    #-#out.extend([cf.getValue()[i] for i in outlocs])
    #-#out.extend([cm[0].getValue()[i] for i in outlocs])
    #-#out.extend([cm[1].getValue()[i] for i in outlocs])
    #-#out.extend([cm[2].getValue()[i] for i in outlocs])
    #-#out.extend([cm[3].getValue()[i] for i in outlocs])
    #-#out.extend([cm[4].getValue()[i] for i in outlocs])
    #-#out.extend([cm[5].getValue()[i] for i in outlocs])
    #-#fh.write(fmtstr % tuple(out))
