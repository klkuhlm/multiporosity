from fipy import CylindricalGrid2D, CellVariable, FaceVariable, TransientTerm, DiffusionTerm, ExponentialConvectionTerm, ImplicitSourceTerm, Viewer
#import matplotlib
#matplotlib.use('Agg')
#import matplotlib.pyplot as plt
import numpy as np

L = 30.0
H = 3.0
f = 0.1
nx = 100
dy = f
ny = int(H/dy)
dx = L/nx

phif = 0.01   # fracture porosity
phim = 0.09  # matrix porosity

gamma = 0.5   # fracture-matrix head transfer rate
#beta = [0.2,0.2,0.3]  # fracture-matrix conc transfer rate

D = 1.0E-1   # fracture diffusion coeff
Rf = 1.0     # fracture retardation coefficient
Rm = 1.0     # matrix retardation coefficient

betaT = 1.0  #phim*Rm/(phif*Rf)
DR = D/Rf

Kf = 1.0       # fracture permeability
Ssf = 5.0E-2   # fracture specific storage

m = CylindricalGrid2D(dx=dx, dy=dy, nx=nx, ny=ny)

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# pseudo steady-state dual porosity flow solution 
# analogous dual porosity transport solution 

# head/drawdown in fractures/matrix
p = CellVariable(name='$p$', mesh=m, value=0.0)

# specified flux at one end/ constant head at other
specFlux = 1.0E0
p.faceGrad.constrain([specFlux,], m.facesLeft)
p.faceGrad.constrain([0.0,], m.facesRight)

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# concentration in fractures and matrix

## non-zero initial concentration section of domain
#c0 = np.zeros(nx, 'd')
##c0[20:50] = 1.0
c0 = np.linspace(0.0,1.0,nx)
#c0 = np.ones(nx, 'd')

c = CellVariable(name="$c$", mesh=m, value=c0)  # fresh water in fractures?

c.faceGrad.constrain([specFlux], m.facesLeft)
c.faceGrad.constrain([0.0,], m.facesRight)

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# flow equation
Peqn = TransientTerm(Ssf,var=p) == DiffusionTerm(Kf,var=p)

# transport equation (includes convection in matrix)
Ceqn =  (TransientTerm(1.0,var=c) == DiffusionTerm(DR,var=c) - 
         ExponentialConvectionTerm(-p.faceGrad,var=c))

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Hviewer = Viewer(vars=(p,),title='Pressure',legend=4,datamin=-0.3,datamax=0)
Hviewer.plot()
Cviewer = Viewer(vars=(c,),datamax=1.0,datamin=0.0,title='Concentration',legend=4)
Cviewer.plot()

time = 0.0
split = 3.0E-1
dt = np.concatenate((np.logspace(-4,np.log10(split),20),np.ones((200,))*split),axis=1)

#-#fh = open('solution-double-porosity-flow-and-transport.dat','w')
#-#fmtstr = ','.join('%.5f' for i in range(len(outlocs)*9+1)) + '\n'

##saveFigs = False

###x = m.cellCenters[0] # for plotting
###c = ['g','r','m','c','k','y']

for j,timeStep in enumerate(dt):
    time += timeStep
    print j,time

    ###fig = plt.figure(1,figsize=(11,7))
    ###hax = fig.add_subplot(211)
    ###cax = fig.add_subplot(212)

    Peqn.solve(dt=timeStep)
    ###hax.plot(x,pf.getValue(),'b-',linewidth=2.0,label='fracture')
    ###hax.plot(x,pm.getValue(),'g--',label='matrix')
    ###hax.legend(loc=4)
    ###hax.set_ylim([-4,0])
    ###hax.set_ylabel('downhole pressure')
    ###hax.set_title('pressure change')

    Hviewer.plot()

    #-#out = [time,]
    #-#out.extend([pf.getValue()[i] for i in outlocs])
    #-#out.extend([pm.getValue()[i] for i in outlocs])

    Ceqn.solve(dt=timeStep)
    ###cax.plot(x,cf.getValue(),'b-',linewidth=2.0,label='fracture')
    ###for k in range(6):
    ###    cax.plot(x,cm[k].getValue(),'%s--'%c[k],label='matrix %i'%k)

    ###cax.legend(loc=4)
    ###cax.set_ylim([0,1])
    ###cax.set_ylabel('concentration')
    ###cax.set_xlabel('distance from borehole [m]')
    ###cax.set_title('concentration')
    ###
    ###plt.suptitle('t=%.2g' % time)
    ###plt.savefig('combined-%4.4i.png' % j)
    ###plt.close(1)

    Cviewer.plot()

    #-#out.extend([cf.getValue()[i] for i in outlocs])
    #-#out.extend([cm[0].getValue()[i] for i in outlocs])
    #-#out.extend([cm[1].getValue()[i] for i in outlocs])
    #-#out.extend([cm[2].getValue()[i] for i in outlocs])
    #-#out.extend([cm[3].getValue()[i] for i in outlocs])
    #-#out.extend([cm[4].getValue()[i] for i in outlocs])
    #-#out.extend([cm[5].getValue()[i] for i in outlocs])
    #-#fh.write(fmtstr % tuple(out))
