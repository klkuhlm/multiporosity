from matplotlib import pyplot as plt
import numpy as np
from subprocess import call

input_string = """T 1.0 T 10.0 0.0 0                   :: specQ?, rD, inf?, xfD, H, debug 
%.5g  %.5g  1.0  1.0 0.0 0.0              :: kappa, omegaf, yeD, alpha, Rf, Rm
2                               :: model (2=lognormal, 10=slab diffusion, 101=dual porosity, etc.)
11  %.5g  %.5g                     :: continuous model params (model=1): quadrature order, par(1:4)
250                               :: slab diffusion (model=10): N (~inf series)
1.0                              :: arbitrary series (model>100) abcissa (101=1 term, 102=2 terms ...)
1.0                              :: arbitrary series (model>100) pdf (101=1 term, 102=2 terms ...) 
1.0                              :: arbitrary series (model>100) omegaj (101=1 term, 102=2 terms ...) 
T  -9.0 6.0 100 not.used         :: calcT?, min log(t), max log(t), # times, time input filename 
1.0E-8 1.0E-8 20                 :: de Hoog et al. params: alpha, tolerance, M
test.out                         :: output filename
0                                 :: time index
"""

KD_orig = 0.01
KD = KD_orig
KDvec = np.logspace(-10,1,4)

omega_orig = 1.0E-6
omega = omega_orig
omegavec = np.logspace(-10,-1,4)

mu_orig = -3.0
mu = mu_orig
muvec = np.linspace(-6,6,4)

sigma_orig = 2.0
sigma = sigma_orig
sigmavec = np.logspace(-1,3,4)

colors = ['r','g','b','c','k','m','y']
markers = ['.','o','^','*','s','h']

fig = plt.figure(1,figsize=(25,25))

exe = './multiporosity'

for j,(vec2,varname) in enumerate(zip((omegavec,muvec,sigmavec),('$\\omega$','$\\mu$','$\\sigma$'))):
    sublist = [KD,omega,mu,sigma]
    ax = fig.add_subplot(4,4,j+2)
    dax = fig.add_subplot(4,4,4*(j+1)+1)
    for c,v1 in zip(colors,KDvec):
        for m,v2 in zip(markers,vec2):
            print 'kappa:',v1,varname.lstrip('$\\').rstrip('$')+':',v2
            fh = open('multiporosity.in','w')
            sublist[0] = v1
            sublist[j+1] = v2
            fh.write(input_string % tuple(sublist))
            fh.close()
            call(exe)
        
            t,z,dz = np.loadtxt('test.out',skiprows=6,unpack=True)
            ax.loglog(t,z,color=c,linestyle='-',marker=m,
                      markersize=3,markeredgecolor=c,markerfacecolor=c)
            dax.loglog(t,dz,color=c,linestyle='--',marker=m,
                      markersize=3,markeredgecolor=c,markerfacecolor=c)
            ax.set_title('$\\psi$ $\\kappa$ (colors) vs. '+varname+' (markers)')
            dax.set_title('$\\partial \\psi$  $\\kappa$ (colors) vs. '+varname+' (markers)')
            
        omega = omega_orig
        mu = mu_orig
        sigma = sigma_orig
    ax.set_ylim([0.1,40])

KD = KD_orig
omega = omega_orig
mu = mu_orig
sigma = sigma_orig

for j,(vec2,varname) in enumerate(zip((muvec,sigmavec),('$\\mu$','$\\sigma$'))):
    sublist = [KD,omega,mu,sigma]
    ax = fig.add_subplot(4,4,j+7)
    dax = fig.add_subplot(4,4,(j+2)*4+2)
    for c,v1 in zip(colors,omegavec):
        for m,v2 in zip(markers,vec2):
            print 'omega:',v1,varname.lstrip('$\\').rstrip('$')+':',v2
            fh = open('multiporosity.in','w')
            sublist[1] = v1
            sublist[j+2] = v2
            fh.write(input_string % tuple(sublist))
            fh.close()
            call(exe)
        
            t,z,dz = np.loadtxt('test.out',skiprows=6,unpack=True)
            ax.loglog(t,z,color=c,linestyle='-',marker=m,
                      markersize=3,markeredgecolor=c,markerfacecolor=c)
            ax.set_title('$\\psi$ $\\omega$ (colors) vs. '+varname+' (markers)')
            dax.loglog(t,dz,color=c,linestyle='-',marker=m,
                      markersize=3,markeredgecolor=c,markerfacecolor=c)
            dax.set_title('$\\partial \\psi$ $\\omega$ (colors) vs. '+varname+' (markers)')
            
        mu = mu_orig
        sigma = sigma_orig
    ax.set_ylim([0.1,40])

KD = KD_orig
omega = omega_orig
mu = mu_orig
sigma = sigma_orig

sublist = [KD,omega,mu,sigma]
ax = fig.add_subplot(4,4,12)
dax = fig.add_subplot(4,4,15)
for c,v1 in zip(colors,muvec):
    for m,v2 in zip(markers,sigmavec):
        print 'mu:',v1,'sigma:',v2
        fh = open('multiporosity.in','w')
        sublist[2] = v1
        sublist[3] = v2
        fh.write(input_string % tuple(sublist))
        fh.close()
        call(exe)
        
        t,z,dz = np.loadtxt('test.out',skiprows=6,unpack=True)
        ax.loglog(t,z,color=c,linestyle='-',marker=m,
                  markersize=3,markeredgecolor=c,markerfacecolor=c)
        ax.set_title('$\\psi$ $\\mu$ (colors) vs. $\\sigma$ (markers)')
        dax.loglog(t,dz,color=c,linestyle='-',marker=m,
                  markersize=3,markeredgecolor=c,markerfacecolor=c)
        dax.set_title('$\\partial \\psi$ $\\mu$ (colors) vs. $\\sigma$ (markers)')

ax.set_ylim([0.1,40])

plt.tight_layout()
plt.savefig('multiporosity-flow-matrix-lognormal.pdf')
