import numpy as np
import matplotlib.pyplot as plt
from subprocess import call

input_string = """True  1.0D0  True  10.0D0 0.0 0          :: specQ?, rD, inf?, xfD, H, debug
%.5e  %.5e  1.0  1.0  0.0  0.0      :: kappa, omegaf, yeD (block length), alpha, Rf, Rm (desorbtion ratios)
10                               :: model (1=exp, 2=lognorm, 3=gamma, 10=slab diffusion, 101=dual porosity, etc.)
7 0.5  2.0  0.0  100.0           :: CONTINUOUS PDF (model<10): quadrature order, pdf model parameters 1: up to 4
%i                            :: INFINITE SERIES DIFFUSION (model>=10): N (~inf series)
1.0E-9                  :: ARBITRARY SERIES (model>100) abcissa (101=1 term, 102=2 terms ...)
1.0101                  :: ARBITRARY SERIES (model>100) pdf (101=1 term, 102=2 terms ...) 
1.0                    :: ARBITRARY SERIES (model>100) omegaj (101=1 term, 102=2 terms ...)                  
True  -1.0 9.0 200 not.used                :: calcT?, min log(t), max log(t), # times, time input filename 
1.0E-8 1.0E-8 15                 :: de Hoog et al. params: alpha, tolerance, M
test.out          :: output filename
0                :: time behavior
"""

fig = plt.figure(1,figsize=(15,10))
ax1 = fig.add_subplot(211)
ax2 = fig.add_subplot(212)

exe = './multiporosity'

lines = ['-',':','--']
colors = ['r','g','b','c','k','m','y']

for n,color in zip([1,2,5,10,30,100,300],colors):
    for kappa,line in zip([1.0E-3,1.0E-6,1.0E-9],lines):
        fh = open('multiporosity.in','w')
        fh.write(input_string % (kappa,0.001,n))
        fh.close()
        call(exe)

        if color == 'k':
            tt,zz,dzz = np.loadtxt('test-analytical-slab%i.out' % (np.abs(np.log10(kappa))),
                                   skiprows=10,unpack=True) 
            ax1.semilogx(tt,zz,color=color,linestyle='None',marker='.')
            ax2.loglog(tt,dzz,color=color,linestyle='None',marker='.')

        t,z,dz = np.loadtxt('test.out',skiprows=10,unpack=True)
        ax1.semilogx(t,z,color=color,linestyle=line)
        ax2.loglog(t,dz,color=color,linestyle=line,label='$\\kappa=%.3g$, $n=%i$' % (kappa,n))

ax1.set_ylim([12,0])
ax1.grid()
ax2.legend(loc=0,prop={'size':9},ncol=len(lines))
plt.savefig('infinite-terms.pdf')
