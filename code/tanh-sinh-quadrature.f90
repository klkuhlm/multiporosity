!
! Copyright (c) 2014-2015 Kristopher L. Kuhlman (klkuhlm at sandia dot gov)
!
! Permission is hereby granted, free of charge, to any person obtaining a copy
! of this software and associated documentation files (the "Software"), to deal
! in the Software without restriction, including without limitation the rights
! to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
! copies of the Software, and to permit persons to whom the Software is
! furnished to do so, subject to the following conditions:
!
! The above copyright notice and this permission notice shall be included in
! all copies or substantial portions of the Software.
!
! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
! IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
! FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
! AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
! LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
! OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
! THE SOFTWARE.
!

module tanh_sinh_quadrature

  contains

    !! ###################################################
  subroutine tanh_sinh_setup(k,w,a)

    ! this is setup to work with the interval 0<=x<=infinity

    use constants, only : PIOV2, DP
    implicit none
    
    integer, intent(in) :: k ! order
    real(DP), intent(out), dimension(2**k-1) :: w, a ! weights and abcissa

    integer :: N,r,i
    real(DP) :: h
    real(DP), allocatable :: u(:,:), arg(:)

    !! compute weights 
    N = 2**k-1
    r = (N-1)/2
    h = 4.0/2**k

    allocate(u(2,N),arg(N))
       
    arg = h*[(i, i=-r,r)]
    u(1,1:N) = PIOV2*cosh(arg)
    u(2,1:N) = PIOV2*sinh(arg)
    
    a(1:N) = tanh(u(2,1:N))
    w(1:N) = u(1,1:N)/cosh(u(2,:))**2
    
    deallocate(arg,u)

    w(1:N) = 2.0*w(1:N)/sum(w(1:N))

    ! map the -1<=a<=1 interval of the quadrature method
    ! onto 0<=a<infinity interval of integrand
    a = 2.0/(a + 1.0) - 1.0

  end subroutine tanh_sinh_setup

  !! ###################################################
  ! polynomial extrapolation 
  SUBROUTINE polint(xa,ya,x,y,dy)
    ! xa and ya are given x and y locations to fit an nth degree polynomial
    ! through.  results is a value y at given location x, with error estimate dy

    use constants, only : DP
    IMPLICIT NONE
    REAL(DP), DIMENSION(:), INTENT(IN) :: xa
    complex(DP), dimension(:), intent(in) :: ya
    REAL(DP), INTENT(IN) :: x
    complex(DP), INTENT(OUT) :: y,dy
    INTEGER :: m,n,ns
    COMPLEX(DP), DIMENSION(size(xa)) :: c,d,den
    REAL(DP), DIMENSION(size(xa)) :: ho
    integer, dimension(1) :: tmp

    n=size(xa)
    c=ya
    d=ya
    ho=xa-x
    tmp = minloc(abs(x-xa))
    ns = tmp(1)
    y=ya(ns)
    ns=ns-1
    do m=1,n-1
       den(1:n-m)=ho(1:n-m)-ho(1+m:n)
       if (any(abs(den(1:n-m)) < spacing(0.0))) then
          print *, 'polynomial extrapolation calculation failure'
          stop
       end if
       den(1:n-m)=(c(2:n-m+1)-d(1:n-m))/den(1:n-m)
       d(1:n-m)=ho(1+m:n)*den(1:n-m)
       c(1:n-m)=ho(1:n-m)*den(1:n-m)
       if (2*ns < n-m) then
          dy=c(ns+1)
       else
          dy=d(ns)
          ns=ns-1
       end if
       y=y+dy
    end do
  END SUBROUTINE polint

end module tanh_sinh_quadrature
