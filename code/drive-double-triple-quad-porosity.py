from matplotlib import pyplot as plt
import matplotlib.ticker as mpt
import numpy as np
from subprocess import call

xticks = np.array([-5,-3,-1,1,3,5,7,9])

input_string = """T 1.0 T 10.0 0.0 0        :: specQ?, rD, inf?, xfD, debug
1.0E-3  %.5e  10.0  1.0 0.0 0.0         :: kappa, omegaf, yeD, alpha, Rf, Rm
%i                                :: model (e.g, 1=lognormal, 10=slab diffusion, 101=dual porosity)
11 -1.5  2.0                     :: continuous model params (model=1): quadrature order, par(1:4)
0                               :: slab diffusion (model=10): N (~inf series)
%.5e %.5e %.5e %.5e                  :: arbitrary series (model>100) abcissa (101=1 term, 102=2 terms ...)
%.5e %.5e %.5e %.5e                  :: arbitrary series (model>100) pdf (101=1 term, 102=2 terms ...) 
%.5e %.5e %.5e %.5e                  :: arbitrary series (model>100) omegaj (101=1 term, 102=2 terms ...) 
T  -7.0 10.0 250 not.used         :: calcT?, min log(t), max log(t), # times, time input filename 
1.0E-10 1.0E-11 10                 :: de Hoog et al. params: alpha, tolerance, M
test.out                         :: output filename
0                                 :: time flag
"""
exe = './multiporosity'

Rd = 0.0
fig = plt.figure(1,figsize=(5,5))
ax = fig.add_subplot(111)
#dax = fig.add_subplot(122)

# Analytical solutions for diffusion into matrix for one value of omega
# and three values of kappa, compared to the Warren-Root solution for the same parameters
modelVec = [101, 102, 103, 104]

legendlines = [[],[]]
legendlabels = [[],[]]

kappa = [5.0E-1, 5.0E-3, 5.0E-5, 5.0E-8] # ratio of each matrix to fracture perm
omegaf = 0.001 # fracture portion

for model in modelVec:
    print 'model:',model

    args = [omegaf,model]
    omega = [0,0,0,0]

    if model == 101:
        # Warren-Root double-porosity
        linetype = [1]
        col = 'black'
        label = 'double'
        omega[0] = 1.0 - omegaf
        uvec =   [kappa[0]/omega[0], 0.0, 0.0, 0.0]
        chivec = [1.0, 0.0, 0.0, 0.0]
        omegajvec = [omega[0], 0.0, 0.0, 0.0]

    elif model == 102:
        # WR triple porosity
        linetype = [3,3]
        col = 'red'
        label = 'triple'
        omega[0] = 0.005
        omega[1] = 1.0 - (omegaf + sum(omega))
        uvec = [kappa[0]/omega[0], kappa[1]/omega[1], 0.0, 0.0]
        chivec = [0.5, 0.5, 0.0, 0.0]
        omegajvec = [omega[0], omega[1], 0.0, 0.0]

    elif model == 103:
        # WR quad porosity
        linetype = [6,3]
        col = 'green'
        label = 'quad'
        omega[0] = 0.005
        omega[1] = 0.05
        omega[2] = 1.0 - (omegaf + sum(omega))
        uvec = [kappa[0]/omega[0], kappa[1]/omega[1], kappa[2]/omega[2], 0.0]
        chivec = [1.0/3.0, 1.0/3.0, 1.0/3.0 ,0.0]
        omegajvec = [omega[0], omega[1], omega[2], 0.0]

    elif model == 104:
        # WR penta porosity
        linetype = [9,3]
        col = 'blue'
        label = 'penta'

        omega[0] = 0.005 #0.15 - omegaf
        omega[1] = 0.05 #0.35 - omega[0]
        omega[2] = 0.5 #0.75 - omega[1]
        omega[3] = 1.0 - (omegaf + sum(omega))
        uvec = [kappa[0]/omega[0], kappa[1]/omega[1], kappa[2]/omega[2], kappa[3]/omega[3]]
        chivec = [0.25, 0.25, 0.25, 0.25]
        omegajvec = [omega[0], omega[1], omega[2], omega[3]]


    print 'kappa:',kappa
    print 'omega:',omegaf,omega
    print 'u:',uvec   # a
    print 'chi:',chivec  # pdf
    print 'omegaj:',omegajvec # omegaj

    args.extend(uvec)
    args.extend([1.0,1.0,1.0,1.0]) #chivec)
    args.extend(omegajvec)

    args = tuple(args)

    fh = open('multiporosity.in','w')
    fh.write(input_string % args)
    fh.close()
    call(exe)
    
    t,z,dz = np.loadtxt('test.out',skiprows=6,unpack=True)
    line = ax.loglog(t,np.abs(z),color=col,linestyle='-',linewidth=1.5,marker='None',label=label)[0]

    if len(linetype) > 1:
        line.set_dashes(linetype)

    #line = dax.semilogx(t,dz,color=col,linestyle='-',linewidth=1.5,marker='None')[0]

    #if len(linetype) > 1:
    #    line.set_dashes(linetype)
    #dax.set_title('$\\lambda$ (colors) vs. model (line type)')

ax.legend(loc=0,prop={'size':12},handlelength=3)
# re-arrange legends to be in ~same order as figure (move first item to end)
#legendlines[1].insert(len(legendlines[1]),legendlines[1].pop(0))
#legendlabels[1].insert(len(legendlabels[1]),legendlabels[1].pop(0))

#ax.annotate('model',xy=(0.7,0.35),xycoords='axes fraction')
#ax.annotate('$\\omega=10^{-4}$',xy=(0.7,0.45),xycoords='axes fraction')

#l1 = ax.legend(legendlines[1],legendlabels[1],loc=(0.525,0.05),
#               prop={'size':10},handlelength=3)
#l2 = ax.legend(legendlines[0],legendlabels[0],loc=(0.05,0.75),
#               prop={'size':12},handlelength=3)
#plt.gca().add_artist(l1)

#ax.annotate('matrix/fracture\nperm ratio',xy=(0.05,0.65),xycoords='axes fraction')
#ax.grid()
#dax.grid()
ax.set_ylim([0.6,10])
#dax.set_ylim([0,20])
ax.set_xlabel('$t_D$')
#dax.set_xlabel('$t_D$')

ax.set_xlim([1.0E-4,1.0E+8])
#dax.set_xlim([1.0E-4,1.0E+5])

ax.xaxis.set_major_locator( mpt.LogLocator(base=10.0, numticks=6))
#dax.xaxis.set_major_locator( mpt.LogLocator(base=10.0, numticks=6))

ax.set_ylabel('$\\psi_f^{(q,10)}$')
#dax.set_ylabel('$\\partial Q / (\\ln t_D )$')

plt.tight_layout()
plt.savefig('multiporosity-double-triple-quad-penta.pdf' )
plt.close(1)
