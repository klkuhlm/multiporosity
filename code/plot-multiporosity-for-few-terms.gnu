set logscale x
set xlabel 'tD'
set ylabel 'delta p'

#set term postscript eps enhanced color
#set output 'double-analytical-compar.eps'

set term png
set output 'double-analytical-compare.png'
set key bottom right

plot './test-analytical-slab9.out' using 1:2 title 'analytical lambda=1.0E-9' with lines, \
     './test-analytical-slab6.out' using 1:2 title 'analytical lambda=1.0E-6' with lines, \
     './test-double6.out' using 1:2 title 'WR lambda=1.0E-6' with lines, \
     './test-double9.out' using 1:2 title 'WR lambda=1.0E-9' with lines, \
     './test-analytical-6-1.out' using 1:2 title 'series 1 lambda=1.0E-6' with lines, \
     './test-analytical-6-2.out' using 1:2 title 'series 2 lambda=1.0E-6' with lines, \
     './test-analytical-9-1.out' using 1:2 title 'series 1 lambda=1.0E-9' with lines, \
     './test-analytical-9-2.out' using 1:2 title 'series 2 lambda=1.0E-9' with lines

unset output