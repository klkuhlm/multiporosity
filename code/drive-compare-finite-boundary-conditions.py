from matplotlib import pyplot as plt
import matplotlib.ticker as mpt
import numpy as np
from subprocess import call

ff = np.pi**2/4.0
lmbda = 5.0E-6/ff

input_string="""%s 1.0 %s %s %s 0                    :: specQ?, rD, inf?, xf, H, debug
%.5e  0.0001  1.0  1.0 0.0 0.0         :: kappa, omega, yeD, alpha, Rf, Rm
10                                :: model (e.g, 1=lognormal, 10=slab diffusion, 101=dual porosity)
11 -1.5  2.0                     :: continuous model params (model=1): quadrature order, par(1:4)
1000                               :: slab diffusion (model=10): N (~inf series)
2.02642e-03   2.02642e-03                           :: arbitrary series (model>100) abcissa (101=1 term, 102=2 terms ...)
1.00100e+00   1.00100e+00                          :: arbitrary series (model>100) pdf (101=1 term, 102=2 terms ...) 
1.0           1.0
T  -3.0 9.0 150 not.used         :: calcT?, min log(t), max log(t), # times, time input filename 
1.0E-12 1.0E-12 50                 :: de Hoog et al. params: alpha, tolerance, M
test.out                         :: output filename
0                                 :: time flag
"""

colors = ['red','green','blue','black','orange','magenta','brown','purple','orange']
linetypes = [[1],[12,2],[6,2],[3,2],[1,1],[1]]

exe = './multiporosity'

#legendlines = [[],[]]
#legendlabels = [[],[]]

# compute type I, II, and III outer boundary conditions, compared with infinite case

wellborev = ['F','T']  # flowrate (T) or pressure (F) condition at well?
infv =   ['F',      'F',       'F',        'F',      'F',     'T']
Hv =     ['0.0D+0', '1.0D-4',  '1.0D-2',   '1.0D-1', '1.0D+8', 'inf']
labelv = ['Type II','Type III','Type III','Type III','Type I','infinite']
radiusv = ['1.0D+1','1.0D+2','1.0D+3']

fig = plt.figure(1,figsize=(10,5))
Qax = fig.add_subplot(121)
Pax = fig.add_subplot(122)

legendlines = [[],[]]
legendlabels = [[],[]]

for wellbore in wellborev:
    for linetype,inf,H,label in zip(linetypes,infv,Hv,labelv):
        for radius,color in zip(radiusv,colors):
            fh = open('multiporosity.in','w')
            fh.write(input_string % (wellbore,inf,radius,H,lmbda))
            fh.close()
            call(exe)
                 
            t,z,dz = np.loadtxt('test.out',skiprows=6,unpack=True)
            if inf == 'T':
                # infinite domain
                color = 'black'

            if wellbore == 'T':
                # specified Q
                line = Qax.loglog(t,np.abs(z),color=color,linestyle='-',linewidth=1.5,marker='None')[0]
            else:
                # specified P
                line = Pax.loglog(t,np.abs(z),color=color,linestyle='-',linewidth=1.5,marker='None')[0]
                 
            if len(linetype) > 1 and inf == 'F':
                # dashed line
                line.set_dashes(linetype)

                if radius == radiusv[2] and wellbore == 'T':
                     if  H[-1] == '8':
                         legendlabels[0].append('Type I')
                     else:
                         legendlabels[0].append('$H_D=10^{%i}$' % int(H[-2:]))
     
                     legendlines[0].append(line)

            else:
                # solid  line
                if radius == radiusv[2] and wellbore == 'T' and inf == 'F':
                    legendlabels[0].append('Type II')
                    legendlines[0].append(line)

                if inf == 'F' and wellbore == 'T':
                    legendlabels[1].append('$R_{D}=10^{%i}$' % int(radius[-1]))
                    legendlines[1].append(line)

            if inf == 'T':
                # infinite domain
                if wellbore == 'T' and radius == radiusv[0]:
                    legendlabels[1].append('$\\infty$ domain')
                    legendlines[1].append(line)

            
Qax.legend(legendlines[0],legendlabels[0],loc=(0.025,0.64),prop={'size':11},handlelength=3)
Qax.annotate('$\\omega=10^{-4}$\n$\\lambda=2 \\times 10^{-6}$',xy=(0.05,0.525),xycoords='axes fraction')
Pax.legend(legendlines[1],legendlabels[1],loc=(0.025,0.025),prop={'size':11},handlelength=3)

Qax.set_xlabel('$t_D$')
Pax.set_xlabel('$t_D$')

Qax.set_ylim([1.0,1.0E+4])
Pax.set_ylim([1.0E-3,2])

Qax.xaxis.set_major_locator( mpt.LogLocator(base=10.0, numticks=7))
Pax.xaxis.set_major_locator( mpt.LogLocator(base=10.0, numticks=7))

Qax.set_ylabel('$\\psi_f^{(q,R_{D})}$')
Pax.set_ylabel('$q_f^{(P,R_{D})}$')

plt.tight_layout()
plt.savefig('multiporosity-outer-boundary-conditions.pdf')
plt.close(1)
