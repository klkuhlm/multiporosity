!
! Copyright (c) 2014-2015 Kristopher L. Kuhlman (klkuhlm at sandia dot gov)
!
! Permission is hereby granted, free of charge, to any person obtaining a copy
! of this software and associated documentation files (the "Software"), to deal
! in the Software without restriction, including without limitation the rights
! to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
! copies of the Software, and to permit persons to whom the Software is
! furnished to do so, subject to the following conditions:
!
! The above copyright notice and this permission notice shall be included in
! all copies or substantial portions of the Software.
!
! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
! IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
! FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
! AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
! LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
! OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
! THE SOFTWARE.
!

program test

  use constants, only : DP
  use utility, only : linspace, logspace
  implicit none

  integer :: i,j
  real(DP), dimension(5) :: alpha,beta,k,theta
  real(DP), dimension(3) :: mu,sigma,lambda
  real(DP), dimension(100) :: x

  ! beta distribution (comparison to Wikipedia figs)
  ! ########################################
  alpha = [0.5, 5.0, 1.0, 2.0, 2.0]
  beta = [0.5, 1.0, 3.0, 2.0, 5.0]
  
  x = linspace(0.0_DP,1.0_DP,size(x))
  
  open(20,file='beta.out',action='write',status='replace')

  do i=1,size(x)
     write(20,*) x(i),(beta_pdf(x(i),alpha(j),beta(j),0.0_DP,1.0_DP),j=1,size(alpha))
  end do
  
  close(20)

  ! gamma distribution (comparison to wikipedia figs)
  ! ########################################
  k = [1.0, 2.0, 3.0, 5.0, 9.0]
  theta = [2.0, 2.0, 2.0, 1.0, 0.5]

  x = linspace(0.0_DP,20.0_DP,size(x))

  open(20,file='gamma.out',action='write',status='replace')

  do i=1,size(x)
     write(20,*) x(i),(gamma_pdf(x(i),k(j),theta(j)),j=1,size(k))
  end do
 
  close(20)


  ! lognormal distribution (comparison to wolfram alpha)
  ! ########################################
  mu = [-2.0, 0.0, 0.1]
  sigma = [0.707107, 0.447214, 0.547723]

  x = linspace(0.0_DP,3.0_DP,size(x))

  open(20,file='ln.out',action='write',status='replace')

  do i=1,size(x)
     write(20,*) x(i),(lognormal_pdf(x(i),mu(j),sigma(j)),j=1,size(mu))
  end do

  close(20)

  ! exponential distribution (comparison to wikipedia)
  ! ########################################
  lambda = [0.5,1.0,1.5]

  x = linspace(0.0_DP,5.0_DP,size(x))

  open(20,file='exp.out',action='write',status='replace')

  do i=1,size(x)
     write(20,*) x(i),(exp_pdf(x(i),lambda(j)),j=1,size(lambda))
  end do

  close(20)


  contains

  elemental function lognormal_pdf(x,mean,stdev) result(pdf)
    ! defined 0 < x < infinity
    use constants, only : DP, RT2PI, RT2
    real(DP), intent(in) :: x,mean,stdev
    real(DP) :: pdf
    
    pdf = exp(-(log(x) - mean)**2/(2*stdev**2))/(x*stdev*RT2PI)

  end function lognormal_pdf


  elemental function gamma_pdf(x,shape,scale) result(pdf)
    ! defined 0 < x < infinity
    ! both shape and scale must be positive (>0)
    use constants, only : DP
    real(DP), intent(in) :: x,shape,scale
    real(DP) :: pdf
    intrinsic :: gamma

    pdf = x**(shape-1)*exp(-x/scale)/(gamma(shape)*scale**shape)

  end function gamma_pdf


  elemental function beta_pdf(x,alpha,beta,min,max) result(pdf)
    ! defined min <= x <= max
    ! both shape parameters (alpha,beta) must be positive (>0)
    use constants, only : DP
    real(DP), intent(in) :: x,alpha,beta,min,max
    real(DP) :: pdf, BF
    intrinsic :: gamma
    
    BF = gamma(alpha)*gamma(beta)/gamma(alpha+beta)
    pdf = x**(alpha-1)*(1-x)**(beta-1)/BF
    pdf = min + pdf*abs(max-min)

  end function beta_pdf

  elemental function exp_pdf(x,lambda) result(pdf)
    ! defined on 0 < x < infinity
    ! lambda > 0
    use constants, only : DP
    real(DP), intent(in) :: x, lambda
    real(DP) :: pdf

    pdf = lambda*exp(-lambda*x)

  end function exp_pdf
  

end program test
