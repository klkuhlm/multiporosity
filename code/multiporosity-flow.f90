!
! Copyright (c) 2014-2015 Kristopher L. Kuhlman (klkuhlm at sandia dot gov)
!
! Permission is hereby granted, free of charge, to any person obtaining a copy
! of this software and associated documentation files (the "Software"), to deal
! in the Software without restriction, including without limitation the rights
! to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
! copies of the Software, and to permit persons to whom the Software is
! furnished to do so, subject to the following conditions:
!
! The above copyright notice and this permission notice shall be included in
! all copies or substantial portions of the Software.
!
! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
! IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
! FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
! AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
! LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
! OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
! THE SOFTWARE.
!

program multiporosity

  use constants, only : DP, EP, PI, PISQ
  use invlap
  use utility, only : logspace, ctanh, operator(.SUM.) ! outer sum 
  use tanh_sinh_quadrature, only : tanh_sinh_setup, polint
  use complex_bessel, only : cbesk, cbesi
  
  implicit none

  real(DP) :: rD, kappa, omegaf, omomega, yeD, xfD, H, alpha, beta
  real(DP), dimension(4) :: pdfpars
  logical :: specQ, calcT, inf
  real(DP) :: minlogt, maxlogt, tee, Rf, Rm, Rfhat
  integer :: nt, np, j, n, m, Ns, Nd, debug
  integer :: nz, ierr, model, timeFlag
  real(DP), allocatable :: timePar(:)
  character(128) :: outfn, timefn
  character(21) :: fmt
  complex(DP), dimension(0:1) :: K, I, IR, KR
  real(DP) :: ft, dft, gamma, slope, slope0, Sumslope, intercept
  real(DP), allocatable :: t(:), pdf(:), omegaj(:)
  complex(DP), allocatable :: p(:), fp(:), g(:), eta(:), barft(:)
  type(invLaplace) :: lap

  real(DP), allocatable :: w(:), a(:)  ! weights and abcissa 
  real(DP), allocatable :: hh(:)  ! heirachy of quadrature spacings
  integer, allocatable :: kk(:), NN(:), ii(:)
  complex(DP), allocatable :: tmp(:,:)
  integer :: nex, na, kmax
  complex(DP) :: polerr
  real(DP) :: x,dx

  ! extended precision stuff only appears in finite domain solutions
  real(EP) :: argIr, argI
  complex(EP) :: xi, zeta, argK, argKr
  complex(EP), dimension(0:1) :: KE, IE, IRE, KRE

  intrinsic :: dbesj0, dbesj1

  open(unit=20,file='multiporosity.in',action='read',status='old')

  ! specQ = specified Q? 
  ! rD = observation radius; inside well (=1) for now
  ! inf = infinite domain?
  ! xfD = dimensionless fracture radius (radial extent of domain)
  ! H = dimensionless coefficient in Type III BC @ xfD
  ! debug = integer debug-printing level
  read(20,*,iostat=ierr) specQ,rD,inf,xfD,H,debug
  
  ! kappa = matrix/fracture permeability ratio
  ! omegaf = phi_f*c_f/(phi_tot*c_tot) ratio of
  !         porosity*compressibility of single component to total 
  ! yeD = dimensionless matrix block half width 
  ! dimensionless wellbore storage, dimensionless wellbore skin
  ! dimensionless desorption storability ratio (Kuckuk & Sawyer, 1980)
  read(20,*,iostat=ierr) kappa, omegaf, yeD, alpha, Rf, Rm

  ! index chosing multiporosity model 
  read(20,*) model 

  !! continuous distribution (model<10)
  !! 1 = exponential (1 param)
  !! 2 = lognormal (2 param)
  !! 3 = gamma (2 param)
  !! 4 = beta (4 param)
  !! -------------------------------------------

  pdfpars(1:4) = -999. 
  kmax = -999

  select case (model)
  case (1)
     read(20,*) kmax, pdfpars(1)
  case (2:3)
     read(20,*) kmax, pdfpars(1:2)
  case (4)
     read(20,*) kmax, pdfpars(1:4)
  case default
     read(20,*)  ! not used, read anyway
  end select
  
  !! diffusion series  (model>=10 && model<100)
  !! ----------------------------------
  ! # terms in series (not used for model<10 or model>100)
  ! Ns == 0 for analytical diffusion solution 
  read(20,*) Ns 

  !! arbitrary discrete model (model>100)
  !! ------------------------------------
  if (model > 100) then
     Nd = model-100 ! number of discrete terms
     allocate(a(Nd),pdf(Nd),omegaj(Nd))
     read(20,*) a(1:Nd)
     read(20,*) pdf(1:Nd)
     read(20,*) omegaj(1:Nd)

     ! initialize unused parameters for
     ! continuous distributions (not used)
     nex = -999
     Ns = -999
     allocate(w(0),ii(0),kk(0),NN(0),hh(0),tmp(0,0))
  else
     read(20,*) !a           ! not used, read anyway
     read(20,*) !pdf
     read(20,*) !omegaj
     allocate(omegaj(0))  ! a and pdf are reused elsewhere
  end if

  ! calcT = compute time vector (or read in vector if False)
  ! min/maxlogt = like used in logspace() numpy function
  ! nt = number of times in vector
  ! timefn = filename to read times from (if calcT == False)
  read(20,*) calcT, minlogt, maxlogt, nt, timefn

  ! deHoog Laplace transform parameters
  read(20,*) lap%alpha, lap%tol, lap%M

  ! filename for output
  read(20,*) outfn

  ! integer flag indicating time behavior (see below)
  read(20,*) timeFlag
  allocate(timePar(2*abs(timeFlag)+1))
  timePar = -999.

  ! timeFlag == 0 simple "step on" at t=0 behavior (timePar not used)
  ! timeFlag < 0 piecewise constant arbitrary time behavior
  ! timeFlag > 0 piecewise linear arbitrary time behevior

  ! for piecewise constant|linear behavior, there should be 2n+1 values in 
  ! timePar for n discrete sections.

  ! first n values are initial times for a section, n+1 is final time
  ! n+2:2n+1 are the "strengths" associated with each initial time.
  ! piecewise constant means horizontal lines until next time, while
  ! piecwise linear means a straight line connecting points.

  ! EXAMPLE
  ! to specify a unit-height tent function on t={1,3}, centered on t=2:
  ! timeFlag = +2
  ! timePar = 1.0 2.0 3.0  0.0 1.0
  ! the first 3 values are times, the last 2 numbers are "height", with
  ! the 6th "0.0" assumed to occur at t=3.0

  if (timeFlag /= 0) then
     backspace(20)
     read(20,*) timeFlag,timePar(:)
  end if
  close(20)  

  !! input error checking
  !! ====================================================
  
  if (any([alpha,kappa,omegaf,yeD] <= 0.0)) then
     ! non-positive input parameters
     print *, 'ERROR: parameters [alpha,kappa,omegaf,yeD] must be > 0.0: ',&
          & [alpha,kappa,omegaf,yeD]
     stop 101
  end if

  if (omegaf >= 1.0) then
     print *, 'ERROR: omega cannot be >= 1 (1 == single porosity): '&
          & ,omegaf
     stop 102
  end if

  if (any([Rf, Rm] < 0.0)) then
     print *, 'ERROR: R_r & R_m (dimensionless desorption '//&
          &'storability ratios) cannot be negative: ', Rf, Rm
     stop 1021
  end if

  select case (model) 
  case (1:9)
     ! --------------------------------------------------
     ! continuous distribution case
     ! --------------------------------------------------
     if (kmax < 2) then
        print *, 'ERROR: (model<10) continuous pdf tanh-sinh quadrature '//&
             & 'order must be >1: ',kmax
        stop 103
     end if

     select case (model)
     case (1)
        ! exponential parameter: 1=lambda
        if (pdfpars(1) <= 0.0) then
           print *, 'ERROR: exponential (model=1) lambda not >0: ',&
                &pdfpars(1)
           stop 1040
        end if
     
     case (2)
        ! lognormal parameters: 1=mu, 2=sigma
        if (pdfpars(2) <= 0.0) then
           print *, 'ERROR: lognormal (model=2) param 2 '//&
                &'(stddev) not >0: ',pdfpars(2)
           stop 1041
        end if

     case (3)
        ! gamma parameters: 1=shape, 2=scale
        if (any(pdfpars(1:2) <= 0.0)) then
           print *, 'ERROR: gamma (model=3) params 1:2 '//&
                &'(shape,scale) not >0: ',pdfpars(1:2)
           stop 1042
        end if

     case (4)
        ! beta parameters: 1=alpha, 2=beta, 3=min, 4=max
        if (any(pdfpars(1:2) <= 0.0)) then
           print *, 'ERROR: beta (model=4) params 1:2 '//&
                &'(alpha,beta) not >0: ',pdfpars(1:2)
           stop 1043
        elseif (pdfpars(4)-pdfpars(3) <= 0.0) then
           print *, 'ERROR: beta (model=4) params 3:4 '//&
                &'(min,max) not ordered correctly: ',pdfpars(3:4)
           stop 1044
        end if
     case (5:9)
        print *, 'ERROR: discrete models 5-9 not implemented: ',model
        stop 1030
     end select
  case (10:99)
     ! --------------------------------------------------
     ! slab diffusion distribution case
     ! --------------------------------------------------
     select case (model)
     case (11)
        if (Ns == 0) then
           print *, 'ERROR: analytical solutions (Ns=0) not '//&
                &'implemented for cylinder (model=11)'
           stop 555
        end if
     case (13:99)
        print *, 'ERROR: slab diffusion geometries beside slab (10), '//&
             &'cylinder (11), and sphere (12) not implemented:',model
        stop 1051
     end select
     if (Ns < 0) then
        print *, 'ERROR: (10<=model<100) slab diffusion # terms must '//&
             &'be >=0 (0 -> analytical solution): ',Ns
        stop 105
     end if
        
  case (100)
     print *, 'ERROR: arbitrary model requires >= one term (model > 100)'
     stop 1071
  case (101:)
     ! --------------------------------------------------
     ! arbitrary porosities case
     ! --------------------------------------------------
    if (any([a,pdf] < 0.0)) then
      print *, 'ERROR: arbitrary model abcissa/pdf must be >0.'
      print *, ' a vector: ', a
      print *, ' pdf vector: ',pdf
      stop 107
    end if
  case (-1)
    print *, 'implementing single-porosity model'
  end select

  if (rD < 1.0) then
     ! observation point inside wellbore (or possibly negative)
     print *,  '=================================================='
     print *,  '   WARNING: rD < 1.0 will be reset to 1.0: ', rD
     print *,  '=================================================='
     rD = 1.0
  end if
  
  if (.not. inf) then
     ! << xfD not used in infinite domain problems>> 
     if (xfD <= 1.0) then
        print *, 'ERROR: dimensionless finite domain radius must be > 1',xfD
        stop 108

     elseif (rD > xfD) then
        ! dimless observation point radius outside dimless domain boundary
        print *,  '=================================================='
        print *,  '   WARNING: rD > xfD will be reset to xfD:',rD
        print *,  '=================================================='
        rD = xfD
     end if
  end if
  
  if (any([lap%alpha,lap%tol] <= 0.0)) then
     print *, 'ERROR: laplace transform [alpha,tol] must be >0:',&
          &[lap%alpha,lap%tol]
     stop 109
  end if

  if (lap%M < 1) then
     print *, 'ERROR: # terms in Laplace-transform inversion <1:',lap%M
     stop 110
  end if

  if (nt <= 0) then
     print *, 'ERROR: number of times to compute or read from file '//&
          &'must be >0: ',nt
     stop 1101
  end if

  if (calcT .and. minlogt >= maxlogt) then
     print *, 'ERROR: minlogt must be < maxlogt: ',minlogt,maxlogt
     stop 1102
  end if

  allocate(t(nt))
  if (calcT) then
     t = logspace(minlogt,maxlogt,nt)
  else
     open(unit=50,file=timefn,action='read',status='old')
     do n=1,nt
        read(50,*) t(n)
     end do
     if (any(t <= 0.0)) then
        print *, 'ERROR: times read from',timefn,'must all be >0:'
        print *, 't vector: ',t
        stop 1103
     end if
  end if

  ! DEBUGGING / checking input
  ! write out time behavior for quick plotting check
  if (debug > 1 .and. timeFlag /= 0) then
     open(unit=77,file='time_behavior_check.dat',&
          &action='write',status='replace')
     
     select case (timeFlag)
     case(:-1)
        ! piecewise constant time behavior
        n = -timeFlag
        ! before first time
        do m=1,nt
           if (t(m) < timePar(1)) then
              write(77,*) t(m),0.0
           end if
        end do
        ! during 1 to n steps
        do j=1,n
           do m=1,nt
              if (t(m) >= timePar(j) .and. t(m) <= timePar(j+1)) then
                 write(77,*) t(m),timePar(n+j+1)
              end if
           end do
        end do
        ! after last step
        do m=1,nt
           if (t(m) > timePar(n)) then
              write(77,*) t(m),0.0
           end if
        end do
     case(1:)
        ! piecewise linear time behavior
        n = timeFlag
        ! before first time
        do m=1,nt
           if (t(m) < timePar(1)) then
              write(77,*) t(m),0.0
           end if
        end do

        ! during 1 to n-1 steps
        slope0 = 0.0
        sumslope = 0.0
        do j=1,n-1
           slope = (timePar(n+j+2) - timePar(n+j+1))/ &
                 & (timePar(j+1) - timePar(j))
           intercept = -(slope - slope0)*timePar(j)
           do m=1,nt
              if (t(m) >= timePar(j) .and. t(m) <= timePar(j+1)) then
                 write(77,*) t(m),slope*t(j) + intercept
              end if
           end do
           slope0 = slope
           sumslope = sumslope + slope
        end do
        ! nth step
        slope = -timePar(2*n+1)/(timePar(n+1) - timePar(n))
        intercept = -sumslope*timePar(n+1)
        do m=1,nt
           if (t(m) >= timePar(n) .and. t(m) <= timePar(n+1)) then
              write(77,*) t(m),slope*t(n) + intercept
           end if
        end do

        ! after last step
        do m=1,nt
           if (t(m) > timePar(n)) then
              write(77,*) t(m),0.0
           end if
        end do        
     end select
     
     close(77)
  end if

  ! matrix capacity for double-porosity case
  omomega = 1 - omegaf

  ! ~ Warren-Root lambda/(1-omega)  (with rw = yeD)
  ! epsilon = alpha * yeD**2
  gamma = alpha*yeD**2*kappa/(Rm + omomega)

  ! ratio of capacities (double-porosity)  
  beta = omomega/omegaf 

  ! modified adsorbtion ratio for fractures
  Rfhat = Rf/omegaf

  np = 2*lap%M+1  ! number of laplace parameters per time
  allocate(p(np),fp(np),barft(np),g(np),eta(np))

  select case (model)
  case (1:9)
     ! 1-4 = {exponential,lognormal,gamma,beta}

     na = 2**kmax - 1  ! # abcissa at densest order of quadrature
     nex = kmax/2 ! # levels of extrapolation
     allocate(a(na),w(na),pdf(na),ii(na), tmp(na,np), &
          & kk(nex),NN(nex),hh(nex))

     Ns = -999 ! not used in this case

     ! continuous models have arbitrary abcissa determined by 
     ! quadrature scheme (here tanh-sinh quadrature)

     kk(1:nex) = [(kmax-m, m=nex-1,0,-1)]
     NN(1:nex) = 2**kk(:) - 1
     hh(1:nex) = 4.0/(2**kk(:))

     ! compute densest set of abcissa
     call tanh_sinh_setup(kk(nex),w(NN(nex)),a(NN(nex))) 

     select case (model)
     case (1)
        pdf = exp_pdf(a,pdfpars(1))
     case (2)
        pdf = lognormal_pdf(a,pdfpars(1),pdfpars(2))
     case (3)
        pdf = gamma_pdf(a,pdfpars(1),pdfpars(2))
     case (4)
        pdf = beta_pdf(a,pdfpars(1),pdfpars(2),pdfpars(3),pdfpars(4))
     end select

  case (10:99)
     ! diffusion series 10-12 = {slab,cylinder,sphere}

     na = Ns ! number of terms in series
     allocate(a(na),pdf(na))
        
     ! >>> for debugging/checking (not used) >>>
     nex = -999
     Nd = -999
     allocate(w(0),ii(0),kk(0),NN(0),hh(0),tmp(0,0))
     ! <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

     select case (model)
     case (10)
        ! diffusion into a slab (1D cartesian)
        
        forall (n=1:na)
           a(n) = (2*n-1)**2 *PISQ*gamma/4.0
           pdf(n) = 8.0*beta/((2*n-1)**2 *PISQ)  ! \hat{chi}
        end forall

     case (11)
        ! diffusion into a cylinder (1D cylindrical)
        
        do n=1,na
           ! guess for zeros of J0(x)
           x = (n-1 + 0.75)*PI 

           NR: do
              ! Newton-Raphson
              dx = dbesj0(x)/dbesj1(x)
              x = x + dx
              if(abs(dx) < spacing(x)) then
                 exit NR
              end if
           end do NR

           a(n) = x**2 *gamma
           pdf(n) = 4.0*beta/x**2  !\hat{chi}
        end do

     case (12)
        ! diffusion into a sphere (1D spherical)
        
        forall (n=1:na)
           a(n) = n**2 *PISQ*gamma
           pdf(n) = 6.0*beta/(n**2 *PISQ) !\hat{chi}
        end forall
     end select
  case (101:)
     ! arbitrary discrete model
     continue ! already handled
  end select
  
  open(unit=30,file=outfn,action='write',status='replace')
  write(30,'(A,L1,1X,ES10.3)') '# specified Q?, rD, debug: ',specQ,rD  
  write(30,'(A,L1,2(1X,ES10.3))') '# infinite domain?, xfD, H: ',inf,xfD,H
  
  write(30,'(A,7(ES10.3,1X))') '# km/kf, omegaf, yeD, WR lambda, alpha, '//&
       &'Rf Rm (desorption): ', kappa, omegaf, yeD, gamma, alpha, Rf, Rm
  write(30,'(A,I0)',advance='no') '# model: ',model

  select case (model)
  case (1:9)
     write(30,'(A,I0)') ' (continuous pdf), tanh-sinh quadrature order: '&
          &,kmax
     select case (model)
     case (1)
        write(30,'(A,ES10.3)') '# exponential dist; rate: ',&
             & pdfpars(1)
     case (2)
        write(30,'(A,2(ES10.3,1X))') '# lognormal dist; mean, std dev: ',&
             & pdfpars(1:2)
     case (3)
        write(30,'(A,2(ES10.3,1X))') '# gamma dist; shape, scale: ',&
             & pdfpars(1:2)
     case (4)
        write(30,'(A,4(ES10.3,1X))') '# beta dist; alpha,beta,min,max: ',&
             & pdfpars(1:4)
     end select
  case (10:99)
     write(30,'(A,I0,A)')  ' (infinite discrete diffusion: N=',Ns,' )'
     select case (model)
     case (10)
        write(30,'(A)',advance='no') '# slab '
     case (11)
        write(30,'(A)',advance='no') '# cylinder '
     case (12)
        write(30,'(A)',advance='no') '# sphere '
     end select
     write(30,'(A,2(ES10.3,1X))')' diffusion; gamma,beta: ',gamma,beta
  case (101:)
     write(30,'(A,I0,A)') ' (arbitrary discrete pdf: ',Nd,' )'
     fmt = '(1(A,XXX(ES10.3,1X)))'
     write(fmt(6:8),'(I3.3)') Nd
     write(30,fmt) '# abcissa: ',a(1:Nd)
     write(30,fmt) '# pdf:     ',pdf(1:Nd) 
     write(30,fmt) '# omega_j: ',omegaj(1:Nd)
   case (-1)
     write(30,*) 'single porosity model: K=',kappa,'Ss=',omegaf
  end select
  
  write(30,'(A,L1,1X,2(ES10.3,1X),I0,1X,A)') '# calcT?, min/max log t, '//&
       &'#t, tfilename: ', calcT,minlogt, maxlogt, nt,trim(timefn)
  write(30,'(A,2(ES10.3,1X),I0)') '# deHoog alpha,tolerance,M: ',&
       &lap%alpha, lap%tol, lap%M
  write(30,'(A,I0)',advance='no') '# time behavior; flag: ',timeFlag
  if (timeFlag /= 0) then
     fmt = '(A,XXX(ES10.3,1X))'
     write(fmt(4:6),'(I3.3)') abs(timeFlag)
     write(30,fmt,advance='no') ' step inital t: ',timePar(1:abs(timeFlag))
     write(30,'(A,ES10.3)',advance='no')' final t:',timePar(abs(timeFlag)+1)
     write(30,fmt,advance='no') '  step values:',timePar(abs(timeFlag)+2:)
     if (timeFlag < 0) then
        write(30,'(A)') ' (piecewise constant)'
     else
        write(30,'(A)') ' (piecewise linear)'
     end if
  else
     write(30,'(A)') ' (step on @ t=0)'
  end if
  
  if (specQ) then
     write(30,'(A)') '#      t_D                 psi_f(r_D)'//&
          &'         deriv wrt ln(t)'
  else
     write(30,'(A)') '#      t_D                  q_f(r_D) '//&
          &'        deriv wrt ln(t)'
  end if
  write(30,'(A)') '#======================================='//&
       &'=========================='

  do n=1,nt
     tee = t(n)*2.0
     p = deHoog_pvalues(tee,lap)
     barft(1:np) = time_pvect(p,timePar(:),timeFlag)

     select case (model)
     case (1:9)
        ! 1-4 = {exponential,lognormal,gamma,beta}
        ! compute continuous memory function integral
        ! NB: removed gamma
        tmp(nex,1:np) = sum(spread(pdf*a*w,2,np)/(a .SUM. p),dim=1)  

        do j=nex-1,1,-1
           !  only need to re-compute weights for each subsequent step
           call tanh_sinh_setup(kk(j),w(1:NN(j)),a(1:NN(j)))

           ! compute index vector, to slice up solution 
           ! for nex'th turn count regular integers
           ! for (nex-1)'th turn count even integers
           ! for (nex-2)'th turn count every 4th integer, etc...
           ii(1:NN(j)) = [( m* 2**(nex-j), m=1,NN(j) )]

           ! estimate integral with subset of function evaluations
           ! and appropriate weights
           ! NB: removed gamma
           tmp(j,1:np) = sum(spread(pdf(ii(1:NN(j)))* &     
                & a(ii(1:NN(j)))*w(ii(1:NN(j))),2,np) / &
                & (a(ii(1:NN(j))) .SUM. p),dim=1)
        end do

        do j=1,np
           ! use polynomial extrap. to extrapolate to integration step = 0
           call polint(hh(1:nex),tmp(1:nex,j),0.0_DP,g(j),PolErr)
        end do

        eta(1:np) = sqrt(p(:)*omegaf*(1 + Rf + g(:)))

     case (10:99)
        if (Ns > 0) then
           ! compute memory kernel for
           ! ~infinite diffusion sum (10 <= model < 100)
           ! beta already incorporated into pdf (chi) terms above
           g(1:np) = sum(spread(pdf*a,2,np)/(a .SUM. p),dim=1)
           eta(1:np) = sqrt(p(:)*omegaf*(1 + Rfhat + g(:)))
        else 
           ! analytical solutions (model = {10,12})
           select case (model)
           case (10)
              ! diffusion into slab (equation 3-26 from da Prat, p 65)
              ! gamma substituted for lambda in da Praat's equations
              g(1:np) = sqrt(gamma*omomega/(3*p))* &
                   & ctanh(sqrt(3*omomega*p/gamma))
           case (12)
              ! diffusion into sphere (equation 2-1 from da Prat, p 66)
              g(1:np) = gamma*(sqrt(15*omomega*p/gamma)/ &
                   & tanh(1/sqrt(15*omomega/gamma)) - 1)/(5*p)
           end select
           eta = sqrt(p(:)*omegaf*(1 + Rfhat + g(:)/omegaf)) 
        end if
        
     case(101:)
        ! compute memory kernel for
        ! arbitrary discrete function (model > 100)
        ! beta_j = omega_j/omega_f
        ! chi = pdf
        ! u = a
        g(1:np) = sum(spread(pdf*omegaj/omegaf*a,2,np)/(a .SUM. p),dim=1)
        eta(1:np) = sqrt(p(:)*omegaf*(1 + Rfhat + g(:)))
      case (-1)
        eta(1:np) = sqrt(p(:)*kappa/omegaf)
     end select
  
     if (specQ) then
        ! solutions for pressure given specified flowrate 
        if (inf) then

           ! infinite domain / specified flowrate (rD=1)
           ! wellbore storage and skin
           do j = 1,np
              ! use scaled K (kode=2)
              call cbesk(Z=eta(j), FNU=0.0_DP, KODE=2, N=2, CY=K(0:1), &
                   &NZ=nz, IERR=ierr)
              if (ierr /= 0 .and. ierr /= 3) then
                 print '(A,I0,A,"(",ES10.3,",",ES10.3,")")', &
                      & 'ERROR: cbesk K{0,1}(eta) (Q,inf) ierr:',&
                      & ierr,' z:',eta(j)
                 stop 901
              end if
              
              ! infinite domain; 
              ! specified flowrate at wellbore
              ! prediction of pressure
              fp(j) = barft(j)*K(0)/(eta(j)*K(1))
              ! <num/denom scaling factors cancel>
           end do
        elseif (.not. inf) then
           ! finite domain (no-flow Type III BC at xfD)
           ! specified flowrate at borehole
           do j = 1,np

              argKr = eta(j)*xfD
              argIr = abs(real(argKr))
              argK = eta(j)
              argI = abs(real(argK))

              call cbesk(Z=eta(j), FNU=0.0_DP, KODE=2, N=2, CY=K(0:1), &
                   &NZ=nz, IERR=ierr)
              if (ierr /= 0 .and. ierr /= 3) then
                 print '(A,I0,A,"(",ES10.3,",",ES10.3,")")', &
                      & 'ERROR: K{0,1}(eta) (Q,xfD) Type III ierr:',&
                      & ierr,' z:',eta(j)
                 stop 005
              end if
              KE = K

              call cbesi(Z=eta(j), FNU=0.0_DP, KODE=2, N=2, CY=I(0:1), &
                   &NZ=nz, IERR=ierr)
              if (ierr /= 0 .and. ierr /= 3) then
                 print '(A,I0,A,"(",ES10.3,",",ES10.3,")")', &
                      & 'ERROR: I{0,1}(eta) (Q,xfD) Type III ierr:',&
                      & ierr,' z:',eta(j)
                 stop 006
              end if
              IE = I

              call cbesk(Z=eta(j)*xfD, FNU=0.0_DP, KODE=2, N=2, CY=KR(0:1),&
                   & NZ=nz, IERR=ierr)
              if (ierr /= 0 .and. ierr /= 3) then
                 print '(A,I0,A,"(",ES10.3,",",ES10.3,")")', &
                      & 'ERROR: K{0,1}(eta*xfD) (Q,xfD) Type III ierr:',&
                      & ierr,' z:',eta(j)*xfD
                 stop 007
              end if
              KRE = KR

              call cbesi(Z=eta(j)*xfD, FNU=0.0_DP, KODE=2, N=2, CY=IR(0:1),&
                   & NZ=nz, IERR=ierr)
              if (ierr /= 0 .and. ierr /= 3) then
                 print '(A,I0,A,"(",ES10.3,",",ES10.3,")")', &
                      & 'ERROR: I{0,1}(eta*xfD) (Q,xfD) Type III  ierr:',&
                      & ierr,' z:',eta(j)*xfD
                 stop 008
              end if
              IRE = IR

              ! compute scaling factors for Bessel functions 
              ! xi and zeta (less scaling factors)
              xi   = exp(argI - argKr)*(eta(j)*KRE(1) - H*KRE(0))
              zeta = exp(argIr - argK)*(eta(j)*IRE(1) + H*IRE(0))

              ! finite domain; 
              ! specified flowrate at wellbore; 
              ! type III at r=xfD
              ! prediction of pressure
              fp(j) = cmplx(-barft(j)*(xi*IE(0) + zeta*KE(0)) / &
                   & (eta(j)*(xi*IE(1) - zeta*KE(1))),kind=DP)

           end do
        end if
     elseif (.not. specQ) then
        ! solutions for flowrate given specified pressure 
        if (inf) then

           ! infinite domain / specified pressure (rD = 1)
           ! no storage and no skin
           do j = 1,np
              ! use scaled K
              call cbesk(Z=eta(j), FNU=0.0_DP, KODE=2, N=2, CY=K(0:1), &
                   &NZ=nz, IERR=ierr)
              if (ierr /= 0 .and. ierr /= 3) then
                 print '(A,I0,A,"(",ES10.3,",",ES10.3,")")', &
                      & 'ERROR: K{0,1}(eta) (P,inf) ierr:',&
                      & ierr,' z:',eta(j)
                 stop 901
              end if
              
              ! TODO: missing fracture aperture term

              ! infinite domain;
              ! specified pressure at wellbore
              ! prediction of flowrate
              fp(j) = PI*eta(j)*barft(j)*K(1)/K(0)
              ! <scaling factors cancel>
           end do
        elseif (.not. inf) then

           ! more general case w/ type III boundary condition at xfD
           ! finite domain / specified pressure at completion
           do j = 1,np

              argKr = eta(j)*xfD
              argIr = abs(real(argKr))
              argK = eta(j)
              argI = abs(real(argK))

              call cbesk(Z=eta(j), FNU=0.0_DP, KODE=2, N=2, CY=K(0:1), &
                   & NZ=nz, IERR=ierr)
              if (ierr /= 0 .and. ierr /= 3) then
                 print '(A,I0,A,"(",ES10.3,",",ES10.3,")")', &
                      & 'ERROR: K{0,1}(eta) (P,xfD) ierr:',&
                      & ierr,' z:',eta(j)
                 stop 015
              end if
              KE = K

              call cbesi(Z=eta(j), FNU=0.0_DP, KODE=2, N=2, CY=I(0:1), &
                   & NZ=nz, IERR=ierr)
              if (ierr /= 0 .and. ierr /= 3) then
                 print '(A,I0,A,"(",ES10.3,",",ES10.3,")")', &
                      & 'ERROR: I{0,1}(eta) (P,xfD) ierr:', &
                      & ierr,' z:',eta(j)
                 stop 016
              end if
              IE = I

              call cbesk(Z=eta(j)*xfD, FNU=0.0_DP, KODE=2, N=2, CY=KR(0:1),&
                   & NZ=nz, IERR=ierr)
              if (ierr /= 0 .and. ierr /= 3) then
                 print '(A,I0,A,"(",ES10.3,",",ES10.3,")")', &
                      & 'ERROR: K{0,1}(eta*xfD) (P,xfD) ierr:',&
                      & ierr,' z:',eta(j)*xfD
                 stop 017
              end if
              KRE = KR

              call cbesi(Z=eta(j)*xfD, FNU=0.0_DP, KODE=2, N=2, CY=IR(0:1),&
                   & NZ=nz, IERR=ierr)
              if (ierr /= 0 .and. ierr /= 3) then
                 print '(A,I0,A,"(",ES10.3,",",ES10.3,")")', &
                      & 'ERROR: I{0,1}(eta*xfD) (P,xfD) ierr:',&
                      & ierr,' z:',eta(j)*xfD
                 stop 018
              end if
              IRE = IR

              ! xi and zeta (less scaling factors)              
              xi   = exp(argI - argKr)*(eta(j)*KRE(1) - H*KRE(0))
              zeta = exp(argIr - argK)*(eta(j)*IRE(1) + H*IRE(0))

              ! TODO: missing fracture aperture term
              ! finite domain; 
              ! specified pressure at wellbore; 
              ! type III at r=xfD
              ! prediction of flowrate
              fp(j) = cmplx(PI*barft(j)*eta(j)*(xi*IE(1) - zeta*KE(1)) / &
                   &(xi*IE(0) + zeta*KE(0)),kind=DP)

           end do
        end if
     end if

     ft = deHoog_invlap(t(n),tee,fp(:),lap)
     fp = fp*p
     dft = deHoog_invlap(t(n),tee,fp(:),lap)*t(n)        
     write(30,'(3(ES20.12E3,2X))') t(n),ft,dft    

  end do

contains
  ! model 1: one parameter
  elemental function exp_pdf(x,lambda) result(pdf)
    ! defined on 0 < x < infinity
    ! lambda > 0
    use constants, only : DP
    implicit none
    real(DP), intent(in) :: x, lambda
    real(DP) :: pdf

    pdf = lambda*exp(-lambda*x)
  end function exp_pdf

  ! model 2: two parameter
  elemental function lognormal_pdf(x,mean,stdev) result(pdf)
    ! defined 0 < x < infinity
    ! stdev must be positive (>0)
    use constants, only : DP, RT2PI
    implicit none
    real(DP), intent(in) :: x,mean,stdev
    real(DP) :: pdf
    
    pdf = exp(-(log(x) - mean)**2/(2*stdev**2))/(x*stdev*RT2PI)
  end function lognormal_pdf

  ! model 3: two parameter
  elemental function gamma_pdf(x,shape,scale) result(pdf)
    ! defined 0 < x < infinity
    ! both shape and scale must be positive (>0)
    use constants, only : DP
    implicit none
    real(DP), intent(in) :: x,shape,scale
    real(DP) :: pdf
    intrinsic :: gamma

    pdf = x**(shape-1)*exp(-x/scale)/(gamma(shape)*scale**shape)
  end function gamma_pdf

  ! model 4: four parameter
  elemental function beta_pdf(x,alpha,beta,min,max) result(pdf)
    ! defined min <= x <= max
    ! both shape parameters (alpha,beta) must be positive (>0)
    use constants, only : DP
    implicit none
    real(DP), intent(in) :: x,alpha,beta,min,max
    real(DP) :: pdf, BF
    intrinsic :: gamma
    
    BF = gamma(alpha)*gamma(beta)/gamma(alpha+beta)
    pdf = x**(alpha-1)*(1-x)**(beta-1)/BF
    pdf = min + pdf*(max-min)
  end function beta_pdf  

  function time_pvect(p,par,flag) result(mult)
    use constants, only : DP
    use utility, only : operator(.X.)
    implicit none

    complex(DP), dimension(:), intent(in) :: p
    integer, intent(in) :: flag
    real(DP), dimension(:), intent(in) :: par
    complex(DP), dimension(size(p,1)) :: mult

    real(DP), allocatable :: ti(:), y(:), dy(:), W(:), b(:), deltaW(:)
    real(DP), allocatable :: denom(:), numer(:)

    real(DP) :: tf, yf, deltaWf, bf
    integer :: n, np
    np = size(p,1)

    select case (flag)
    case (0)
       ! step on at t=0 (default)
       mult(1:np) = 1/p

    case (:-1)

       !! arbitrary piecewise constant time behavior
       !! with n steps, from ti(1) to tf
       n = -flag
       allocate(ti(n),y(0:n),dy(n))

       ! unpack initial times, pumping rates and final time
       ti(1:n) = par(1:n)
       tf = par(n+1)
       y(0) = 0.0
       y(1:n) = par(n+2:2*n+1)
       dy = y(1:n) - y(0:n-1)
       yf = sum(dy(:))

       mult(1:np) = (sum(spread(dy,2,np)*exp(-ti .X. p),1)-yf*exp(-tf*p))/p

       deallocate(ti,y,dy)

    case (1:)
       !! piecewise linear pumping rate with n steps, from ti(1) to tf
       !! no jumps in value (no vertical slopes)
       n = flag
       allocate(ti(n),W(0:n+1),y(1:n+1),denom(n),numer(n),&
            & b(n),deltaW(n))

       ! unpack initial times, pumping rates and final time
       ti(1:n) = par(1:n)
       tf = par(n+1)
       y(1:n) = par(n+2:2*n+1)

       ! compute slope between each pair of points
       W(0) = 0.0  ! 0 and n+1 are "ghost" slopes for FD calc
       W(n+1) = 0.0 
       y(n+1) = 0.0  ! assumed specified BC at tf
       denom = [ti(2:n),tf] - ti(1:n) ! run
       numer = y(2:n+1) - y(1:n) ! rise
       where (abs(denom) < epsilon(abs(numer)))
          ! ensure no "divide by zero" errors
          denom = denom + epsilon(abs(numer))
       end where
       W(1:n) = numer/denom ! slope = rise/run

       deltaW(1:n) = W(1:n) - W(0:n-1)
       deltaWf = sum(deltaW)

       ! intercept of lines shifted down to start on x-axis
       b(1:n) = -deltaW(1:n)*ti(1:n)
       bf = -deltaWf*tf

       mult(1:np) = sum((spread(deltaW,2,np)/spread(p**2,1,n) + &
            & spread(b,2,np))*exp(-ti .X. p),dim=1) - &
            & (deltaWf/p**2 + bf)*exp(-tf*p)

       deallocate(ti,W,y,denom,deltaW,b)

    end select
  end function time_pvect

end program multiporosity
