from matplotlib import pyplot as plt
import numpy as np
from subprocess import call

input_string = """T 1.0 T 10.0 0.0 0                    :: specQ?, rD, inf?, xfD, H, debug
%.5g  %.5g  %.5g  1.0 0.0 0.0     :: kappa, omegaf, yeD, alpha, Rf, Rm
10                               :: model (e.g, 1=lognormal, 10=slab diffusion, 101=dual porosity)
11 -1.5  2.0                     :: continuous model params (model=1): quadrature order, par(1:4)
500                              :: slab diffusion (model=10): N (~inf series)
1.0                              :: arbitrary series (model>100) abcissa (101=1 term, 102=2 terms ...)
1.0                              :: arbitrary series (model>100) pdf (101=1 term, 102=2 terms ...) 
1.0                              :: arbitrary series (model>100) omegaj (101=1 term, 102=2 terms ...) 
T  -9.0 6.0 200 not.used         :: calcT?, min log(t), max log(t), # times, time input filename 
1.0E-8 1.0E-8 20                 :: de Hoog et al. params: alpha, tolerance, M
test.out                         :: output filename
0                                :: time index
"""

KD_orig = 0.01
KD = KD_orig
KDvec = np.logspace(-8,2,5)

omega_orig = 0.01
omega = omega_orig
omegavec = np.logspace(-6,-1,5)

LD_orig = 10.0
LD = LD_orig
LDvec = np.logspace(-1,5,5)

colors = ['r','g','b','c','k','m','y']
markers = ['.','o','^','*','s','h']

fig = plt.figure(1,figsize=(25,25))

exe = './multiporosity'

for j,(vec2,varname) in enumerate(zip((omegavec,LDvec),('$\\omega$','$L_D$'))):
    sublist = [KD,omega,LD]
    ax = fig.add_subplot(3,3,j+2)
    dax = fig.add_subplot(3,3,3*(j+1)+1)
    for c,v1 in zip(colors,KDvec):
        for m,v2 in zip(markers,vec2):
            print 'kappa:',v1,varname.lstrip('$\\').rstrip('$')+':',v2
            fh = open('multiporosity.in','w')
            sublist[0] = v1
            sublist[j+1] = v2
            fh.write(input_string % tuple(sublist))
            fh.close()
            call(exe)
        
            t,z,dz = np.loadtxt('test.out',skiprows=6,unpack=True)
            ax.semilogx(t,z,color=c,linestyle='-',marker=m,
                      markersize=3,markeredgecolor=c,markerfacecolor=c)
            dax.loglog(t,dz,color=c,linestyle='--',marker=m,
                      markersize=3,markeredgecolor=c,markerfacecolor=c)
            ax.set_title('$\\psi$ $\\kappa$ (colors) vs. '+varname+' (markers)')
            dax.set_title('$\\partial \\psi$  $\\kappa$ (colors) vs. '+varname+' (markers)')
            
        omega = omega_orig
        LD = LD_orig
    ax.set_ylim([0.1,10])

KD = KD_orig
omega = omega_orig
LD = LD_orig

sublist = [KD,omega,LD]
ax = fig.add_subplot(3,3,6)
dax = fig.add_subplot(3,3,8)
for c,v1 in zip(colors,omegavec):
    for m,v2 in zip(markers,LDvec):
        print 'omega:',v1,'LD:',v2
        fh = open('multiporosity.in','w')
        sublist[1] = v1
        sublist[2] = v2
        fh.write(input_string % tuple(sublist))
        fh.close()
        call(exe)
        
        t,z,dz = np.loadtxt('test.out',skiprows=6,unpack=True)
        ax.semilogx(t,z,color=c,linestyle='-',marker=m,
                  markersize=3,markeredgecolor=c,markerfacecolor=c)
        ax.set_title('$\\psi$ $\\omega$ (colors) vs. $L_D$ (markers)')
        dax.loglog(t,dz,color=c,linestyle='-',marker=m,
                  markersize=3,markeredgecolor=c,markerfacecolor=c)
        dax.set_title('$\\partial \\psi$ $\\omega$ (colors) vs. $L_D$ (markers)')

ax.set_ylim([0.1,10])

plt.tight_layout()
plt.savefig('multiporosity-flow-matrix-slab-diffusion.pdf')
