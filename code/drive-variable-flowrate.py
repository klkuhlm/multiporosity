from matplotlib import pyplot as plt
import matplotlib.ticker as mpt
import matplotlib.dates as mpd
import numpy as np
from subprocess import call
from itertools import product

# conversions
PSI2MPA = 6.8948E-3  # psi -> MPa
FT32M3 = 0.0283168   # cubic feet -> cubic meters

# if true, use piecewise constant, otherwise piecewise linear
step = False

# if true, use constant flowrate
constant = False

if step:
    stepmult = -1
else:
    stepmult = +1 

input_string="""T  1.0  T  1000.0  0.0  3        :: specQ?, rD, inf?, xf, H, debug
%.5g  %.5g  %.5g  1.0  %.5g  %.5g         :: kappa, omega, yeD, alpha, Rf, Rm
10                                :: model (e.g, 1=lognormal, 10=slab diffusion, 101=dual porosity)
11 -1.5  2.0                     :: continuous model params (model=1): quadrature order, par(1:4)
0                               :: slab diffusion (model=10): N (~inf series)
%.5g  %.5g  %.5g                  :: arbitrary series (model>100) abcissa (101=1 term, 102=2 terms ...)
%.5g  %.5g  %.5g                  :: arbitrary series (model>100) pdf (101=1 term, 102=2 terms ...) 
%.5g  %.5g  %.5g                  :: arbitrary series (model>100) omegaj (101=1 term, 102=2 terms ...) 
F 7.0  12.0 192 lineart-input.dat         :: calcT?, min log(t), max log(t), # times, time input filename 
1.0E-13 1.0E-14 70                 :: de Hoog et al. params: alpha, tolerance, M
variableQ.out                         :: output filename
"""

Rf = 1.0
##Rfvec = [1.0E-2, 1.0, 1.0E+2, 1.0E+4, 1.0E+6]

kap = 1.0E-7
kapvec =  [1.0E-9, 1.0E-11, 1.0E-13, 1.0E-15, 1.0E-17]
colors = ['red',  'green', 'blue','orange','purple','cyan','magenta']

omega = 1.0E-8
omegavec =  [1.0E-2, 1.0E-5, 1.0E-8]
LD = 1.0
#LDvec = [10.0, 100.0, 1000.0]

linetypes = [[1],[11,2],[7,2],[3,2],[1,1]]

Tc = 0.001566461  # characteristic time (involves estimate of properties)
DAYS2SEC = 24.0*60.0*60.0

# t and relative Q*B for variable flowrate case
tvec = np.array([1.0,   5.0, 35.0, 100.0, 287.0, 288.0, 310.0, 320.0, 472.0, 475.0])
#qvec = np.array([20.0,  40.0, 50.0,  55.0,  60.0,  1.0,  1.0,  60.0,  60.0, 0.0])  # "observed flowrate"
qvec = np.array([5.0,  20.0, 50.0,  55.0,  60.0,  30.0,  30.0,  60.0,  60.0, 0.0])  # "apparent flowrate"

tvec *= DAYS2SEC/Tc
qvec /= 60.0

print 'tvec:',tvec
print 'qvec:',qvec

if constant:
    last_line = '0 '+ ' '.join(['%.5g' for x in range(2*len(tvec)-1)])+'\n'
else:
    last_line = str(stepmult*(len(tvec)-1)) +' '+ ' '.join(['%.5g' for x in range(2*len(tvec)-1)])+'\n'

print 'last_line:','"',last_line,'"'

input_string = input_string + last_line

exe = './multiporosity'

fig = plt.figure(1,figsize=(10,5))
ObsQax = fig.add_subplot(121)
ModPax = fig.add_subplot(122)
                
datafn = './W2-export-bhp.csv'
d = np.loadtxt(datafn,delimiter=',',converters={0:mpd.datestr2num})

# data columns in file (columns 0-5 are original data; 
# 6-11 are computed via compute-bottomhole-pressure.py)
#
# 0  : date  (MM/DD/YY)
# 1  : Q_gas 
# 2  : Q_oil
# 3  : Q_water
# 4  : P_tubing
# 5  : P_casing
# 6  : est. bottomhole pressure (O&J)
# 7  : est. pseudopressure (psi)
# 8  : est. downhole flowrate (Q*B)
# 9  : B
# 10 : est. bottomhole pressure (O&J)
# 11 : m/(BQ) (pseudopressure/bottomhole flowrate)
# 12 : t_a (compressibility-only pseudotime - days)
# 13 : t_ca (compressibility and material balance pseudotime - days)

d[:,12] *= DAYS2SEC/Tc
Qmax = 60.0 ##d[:,8].max()

print 'Qmax:',Qmax

ObsQax.plot(d[:,12],d[:,8]*FT32M3,'r.')  # estimated bottomhole flowrate
ObsQax.plot(tvec,qvec*Qmax*FT32M3,'k-')

##ModPax.plot(d[:,0],d[:,6],'b.') # estimated bottomhole pressure
ModPax.plot(d[:,12],d[:,7]*PSI2MPA,'g.') # estimated normalized pseudopressure
ModPax2 = ModPax.twinx()

for (kap,color),(omega,linetype) in product(zip(kapvec,colors),zip(omegavec,linetypes)):
#for (kap,color),(LD,linetype) in product(zip(kapvec,colors),zip(LDvec,linetypes)):
#for (Rf,color),(LD,linetype) in product(zip(Rfvec,colors),zip(LDvec,linetypes)):

    print kap,LD,omega,Rf
    fh = open('multiporosity.in','w')
    args = [kap,omega,LD,Rf,0.0,1,1,1,2,2,2,3,3,3]
    args.extend(tvec)
    args.extend(qvec[:-1])

    fh.write(input_string % tuple(args))
    fh.close()
    call(exe)

    # specified Q (results are dimensionless press)
    t,z,dz = np.loadtxt('variableQ.out',skiprows=11,unpack=True)

    zmax = z.max()
    zmin = z.min()
    zrange = zmax - zmin
    z += abs(zmin)  # make all pressures positive

#    line = ModPax2.plot(t,z,color=color,linestyle='-',linewidth=1.5,
#                       marker='None',label='$\\kappa=%.2g$ $L_D=%.2g$' % (kap,LD))[0]
#    line = ModPax2.plot(t,z,color=color,linestyle='-',linewidth=1.5,
#                        marker='None',label='$\\kappa=%.2g$ $\\omega=%.2g$' % (kap,omega))[0]
 
    # was (z - zrange)
    line = ModPax2.plot(t,z,color=color,linestyle='-',linewidth=1.5,
                        marker='None',label='$\\kappa=%.2g$ $\\omega=%.2g$' % (kap,omega))[0]

    if len(linetype) > 1:
        line.set_dashes(linetype)

#ModPax2.legend(loc=0,prop={'size':6},handlelength=3)
 
ObsQax.set_xlabel('$t_a$ & $t_D$')
ModPax.set_xlabel('$t_a$ & $t_D$')

#ObsQax.set_xlim([0,2.4E+10])
ObsQax.set_ylim([0.1,8])
#ModPax.set_xlim([0,2.4E+10])
#ModPax.set_ylim([0,12000])
#ModPax2.set_ylim([0,14])

#ObsQax.set_yscale("log")
#ObsQax.set_xscale("log")
#ModPax.set_xscale("log")
#ModPax2.set_xscale("log")

ObsQax.set_ylabel('Downhole Gas Flowrate (BQ) [$10^3$ m$^3$/day]')
ModPax.set_ylabel('$m(p)-m(p_i)$ [MPa]')
ModPax2.set_ylabel('$\\Delta \\psi_f^{(q,\infty)}$')

plt.tight_layout()
plt.savefig('multiporosity-variable-flowrate.pdf')
plt.close(1)
