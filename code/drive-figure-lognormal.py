from matplotlib import pyplot as plt
import numpy as np
from subprocess import call
from itertools import product

input_string = """T 1.0 T 10.0  0.0 0                    :: specQ?, rD, inf?, xfD, H, debug
%.5e  %.5e  1.0 1.0  0.0  0.0           :: kappa, omega, yeD, alpha, Rf, Rm
%i                                :: model (e.g, 1=lognormal, 10=slab diffusion, 101=dual porosity)
10  %.5e  %.5e                     :: continuous model params (model=1): quadrature order, par(1:4)
%i                               :: slab diffusion (model=10): N (~inf series)
1.0                          :: arbitrary series (model>100) abcissa (101=1 term, 102=2 terms ...)
1.0                          :: arbitrary series (model>100) pdf (101=1 term, 102=2 terms ...) 
1.0                          :: arbitrary series (model>100) omegaj (101=1 term, 102=2 terms ...) 
T  -3.0 9.0 150 not.used         :: calcT?, min log(t), max log(t), # times, time input filename 
1.0E-8 1.0E-8 20                 :: de Hoog et al. params: alpha, tolerance, M
test.out                         :: output filename
0                                 :: time flag
"""

ff = np.pi**2/4.0
lmbda = 1.0E-6/ff
omega = 1.0E-4

# Analytical solutions for diffusion into matrix for one value of omega (0.001)
# and three values of lambda, compared to the Warren-Root solution for the same parameters

#lmbdaVec = [5.0E-9, 5.0E-6, 5.0E-3] 
#colors =   ['r',    'g',    'b']

modelVec =  [10, 10,   2]
ntermsVec = [1,  1500, 0]

lnmuVec = [-8.0, -5.0, -2.0, 1.0, 4.0]
colors =  ['r',   'g', 'b', 'c', 'm']
lnsigmaVec = [1.0E-4, 1.0E-3, 0.01, 0.1, 1.0]
lines =      [[3,2], [6,2],[9,2], [12,2],[15,2]]

fig = plt.figure(1,figsize=(10,5))

exe = './multiporosity'

ax = fig.add_subplot(121)
dax = fig.add_subplot(122)

legendlines = [[],[]]
legendlabels = [[],[]]

for model,nterms in zip(modelVec,ntermsVec):

    print "model,nterms:",model,nterms

    if model == 10:
        if nterms == 1:
            # series approximation to W-R
            lin = [1,1]
        if nterms > 100:
            # series approximation to Kazemi
            lin = []

        fh = open('multiporosity.in','w')
        fh.write(input_string % (lmbda,omega,model,1.0,1.0,nterms))
        fh.close()
        call(exe)
        
        t,z,dz = np.loadtxt('test.out',unpack=True)
        line = ax.semilogx(t,z,color='k',linestyle='-',linewidth=1.5,marker='None')[0]
        line.set_dashes(lin)

        line = dax.semilogx(t,dz,color='k',linestyle='-',linewidth=1.5,marker='None')[0]
        line.set_dashes(lin)

    elif model == 2:
        
        for (lin,lnsig),(col,lnmu) in product(zip(lines,lnsigmaVec),zip(colors,lnmuVec)):
        
            print model,":",lnsig,lnmu

            fh = open('multiporosity.in','w')
            fh.write(input_string % (lmbda,omega,model,lnmu,lnsig,nterms))
            fh.close()
            call(exe)
            
            t,z,dz = np.loadtxt('test.out',skiprows=6,unpack=True)
            line = ax.semilogx(t,z,color=col,linestyle='-',linewidth=1.5,marker='None')[0]
            line.set_dashes(lin)
    
##            if len(lin) == 0:
##                # solid lines
##                legendlabels[0].append('$\\lambda=10^{%i}$' % np.log10(lmbda))
##                legendlines[0].append(line)
##                
##            if col == 'b':
##                if model == 10:
##                    if nterms == 0:
##                        legendlabels[1].append('Kazemi')
##                    else:
##                        legendlabels[1].append('series N=%i' % nterms)
##                elif model == 101:
##                    legendlabels[1].append('Warren-Root')
##                legendlines[1].append(line)
    
            #ax.set_title('$\\lambda$ (colors) vs. model (line type)')
    
            line = dax.semilogx(t,dz,color=col,linestyle='-',linewidth=1.5,marker='None')[0]
            line.set_dashes(lin)
            #dax.set_title('$\\lambda$ (colors) vs. model (line type)')

    # re-arrange legends to be in ~same order as figure (move first item to end)
    ##legendlines[1].insert(len(legendlines[1]),legendlines[1].pop(0))
    ##legendlabels[1].insert(len(legendlabels[1]),legendlabels[1].pop(0))
    ##
    ##l1 = ax.legend(legendlines[1],legendlabels[1],loc=(0.525,0.05),
    ##               prop={'size':11},handlelength=3)
    ##l2 = ax.legend(legendlines[0],legendlabels[0],loc=(0.05,0.75),
    ##               prop={'size':12},handlelength=3)
    ##plt.gca().add_artist(l1)

    ##ax.annotate('model',xy=(0.7,0.35),xycoords='axes fraction')
    ##ax.annotate('$\\omega=10^{-4}$',xy=(0.7,0.45),xycoords='axes fraction')
    ##ax.annotate('matrix/fracture\nperm ratio',xy=(0.05,0.65),xycoords='axes fraction')

    ax.set_ylim([0,10.5])
    dax.set_ylim([0.0,0.55])
    ax.set_xlabel('dimensionless time $[t_D]$')
    ax.set_ylabel('dimensionless fracture pressure change $[\\psi_f]$')
    dax.set_xlabel('dimensionless time $[t_D]$')
    dax.set_ylabel('derivative $[\\partial \\psi_f / (\\ln t_D ) ]$')
    
    plt.tight_layout()
    plt.savefig('multiporosity-lognormal-compare-WR-KZ.pdf')


