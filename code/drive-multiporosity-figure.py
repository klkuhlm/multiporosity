from matplotlib import pyplot as plt
import matplotlib.ticker as mpt
import numpy as np
from subprocess import call

xticks = np.array([-5,-3,-1,1,3,5,7,9])

input_string = """%s 1.0 T 10.0 0.0 0        :: specQ?, rD, inf?, xfD, H, debug
%.5e  %.5e  1.0  1.0 %.5e 0.0          :: kappa, omega, yeD, alpha, Rf, Rm
%i                                :: model (e.g, 1=lognormal, 10=slab diffusion, 101=dual porosity)
11 -1.5  2.0                      :: continuous model params (model=1): quadrature order, par(1:4)
%i                                :: slab diffusion (model=10): N (~inf series)
%.5e   %.5e                          :: arbitrary series (model>100) abcissa (101=1 term, 102=2 terms ...)
%.5e   %.5e                          :: arbitrary series (model>100) pdf (101=1 term, 102=2 terms ...) 
1.0D0  1.0D0                         :: arbitrary series (model>100) omegaj (101=1 term, 102=2 terms ...) 
T  -6.0 10.0 150 not.used          :: calcT?, min log(t), max log(t), # times, time input filename 
1.0E-12 1.0E-13 40                 :: de Hoog et al. params: alpha, tolerance, M
test.out                           :: output filename
0                                  :: time flag
"""

omega = 0.0001

exe = './multiporosity'

Rf = 0.0
for BC in ['T','F']:
    fig = plt.figure(1,figsize=(10,5))
    ax = fig.add_subplot(121)
    dax = fig.add_subplot(122)

    ff = np.pi**2/4.0

    # Analytical solutions for diffusion into matrix for one value of omega
    # and three values of kappa, compared to the Warren-Root solution for the same parameters
    kappaVec = [5.0E-9,    5.0E-6,    5.0E-3, 
                5.0E-9,    5.0E-6,    5.0E-3, 
                5.0E-9/ff, 5.0E-6/ff, 5.0E-3/ff, 
                5.0E-9/ff, 5.0E-6/ff, 5.0E-3/ff, 
                5.0E-9/ff, 5.0E-6/ff, 5.0E-3/ff]
    colors =   ['r',   'g',   'b',    
                'r',   'g',   'b',    
                'r',   'g',   'b',   
                'r',   'g',   'b',   
                'r',   'g',   'b']
    modelVec = [10,    10,    10,     
                101,   101,   101,    
                10,    10,    10,    
                10,    10,    10, 
                10,    10,    10]
    ntermsVec = [0,    0,     0,   
                 0,    0,     0,  
                 2,    2,     2,  
                10,   10,    10, 
               500,  500,   500]
    
    legendlines = [[],[]]
    legendlabels = [[],[]]
        
    for kappa,col,model,nterms in zip(kappaVec,colors,modelVec,ntermsVec):
        print 'model:',model, 'kappa:',kappa

        if model == 10:
            if nterms == 0:
                # analytical diffusion solution
                lin = [1,]
            # series approximation to diffusion
            elif nterms == 2:
                lin = [3,2]
            elif nterms == 10:
                lin = [6,2]
            elif nterms > 50:
                lin = [9,2]

        elif model == 101:
            # Warren-Root double-porosity
            lin = [1,1]
        elif model == 102:
            # WR triple porosity
            lin = [4,3]

        fh = open('multiporosity.in','w')
        fh.write(input_string % (BC,kappa,omega,Rf,model,nterms,kappa,kappa,1.0/(1-omega),1.0/(1-omega)))
        fh.close()
        call(exe)
        
        t,z,dz = np.loadtxt('test.out',skiprows=6,unpack=True)
        line = ax.semilogx(t,z,color=col,linestyle='-',linewidth=1.5,marker='None')[0]

        if BC != 'T':
            ax.set_yscale('log')
        if len(lin) > 1:
            line.set_dashes(lin)

        if lin == [1]:
            # solid lines
            legendlabels[0].append('$\\kappa=10^{%i}$' % np.log10(kappa))
            legendlines[0].append(line)
            
        if col == 'b':
            if model == 10:
                if nterms == 0:
                    legendlabels[1].append('Kazemi')
                else:
                    legendlabels[1].append('series N=%i' % nterms)
            elif model == 101:
                legendlabels[1].append('Warren-Root')
            legendlines[1].append(line)

        #ax.set_title('$\\lambda$ (colors) vs. model (line type)')

        if BC != 'T':
            dz *= -1

        line = dax.semilogx(t,dz,color=col,linestyle='-',linewidth=1.5,marker='None')[0]

        if BC != 'T':
            dax.set_yscale('log')

        if len(lin) > 1:
            line.set_dashes(lin)
        #dax.set_title('$\\lambda$ (colors) vs. model (line type)')

    # re-arrange legends to be in ~same order as figure (move first item to end)
    legendlines[1].insert(len(legendlines[1]),legendlines[1].pop(0))
    legendlabels[1].insert(len(legendlabels[1]),legendlabels[1].pop(0))

    if BC == 'T':
        ax.annotate('model',xy=(0.7,0.35),xycoords='axes fraction')
        ax.annotate('$\\omega=10^{-4}$',xy=(0.7,0.45),xycoords='axes fraction')
    
        l1 = ax.legend(legendlines[1],legendlabels[1],loc=(0.525,0.05),
                       prop={'size':10},handlelength=3)
        l2 = ax.legend(legendlines[0],legendlabels[0],loc=(0.05,0.75),
                       prop={'size':12},handlelength=3)
        plt.gca().add_artist(l1)

        ax.annotate('matrix/fracture\nperm ratio',xy=(0.05,0.65),xycoords='axes fraction')

    else:
        ax.annotate('model',xy=(0.7,0.605),xycoords='axes fraction')
        ax.annotate('$\\omega=10^{-4}$',xy=(0.7,0.53),xycoords='axes fraction')

        l1 = ax.legend(legendlines[1],legendlabels[1],loc=(0.525,0.655),
                       prop={'size':11},handlelength=3)
        l2 = dax.legend(legendlines[0],legendlabels[0],loc=(0.625,0.75),
                       prop={'size':12},handlelength=3)

        dax.annotate('matrix/fracture\nperm ratio',xy=(0.625,0.65),xycoords='axes fraction')

    if BC == 'T':
        ax.set_ylim([0,10.5])
        dax.set_ylim([0.0,0.55])
    else:
        ax.set_ylim([0.3,13])
        dax.set_ylim([1.0E-5,10])
    ax.set_xlabel('$t_D$')
    dax.set_xlabel('$t_D$')

    ax.xaxis.set_major_locator( mpt.LogLocator(base=10.0, numticks=9))
    dax.xaxis.set_major_locator( mpt.LogLocator(base=10.0, numticks=9))

    if BC == 'T':
        ax.set_ylabel('$\\psi_f^{(q,\infty)}$')
        dax.set_ylabel('$\\partial \\psi_f^{(q,\infty)} / (\\ln t_D )$')
    
        plt.tight_layout()
        plt.savefig('multiporosity-specQ-flow-diffusion-vs-wr-solutions-Rd-%.3g.pdf' % Rf)
    else:
        ax.set_ylabel('$q_f^{(P,\infty)}$')
        dax.set_ylabel('$-\\partial q_f^{(P,\infty)} / (\\ln t_D )$')
    
        plt.tight_layout()
        plt.savefig('multiporosity-specP-flow-diffusion-vs-wr-solutions-Rd-%.3g.pdf' % Rf)

    plt.close(1)
