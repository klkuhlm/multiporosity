from matplotlib import pyplot as plt
import matplotlib.ticker as mpt
import matplotlib.dates as mpd
import numpy as np
from subprocess import call
from itertools import product

# if true, use stepwise, otherwise piecewise linear
step = True

# if true, use WR dual porosity
use_dual = True

if step:
    stepmult = -1
else:
    stepmult = +1 

input_string_slab="""F  1.0  T  1000.0  0.0  0                    :: specQ?, rD, inf?, xf, H, debug
%.5g  %.5g  %.5g  1.0 0.0 0.0       :: kappa, omegaf, yeD, alpha, Rf, Rm
10                                 :: model (e.g, 1=lognormal, 10=slab diffusion, 101=dual porosity)
11 -1.5  2.0                     :: continuous model params (model=1): quadrature order, par(1:4)
10                               :: slab diffusion (model=10): N (~inf series)
2.02642e-03   2.02642e-03                           :: arbitrary series (model>100) abcissa (101=1 term, 102=2 terms ...)
1.00100e+00   1.00100e+00                          :: arbitrary series (model>100) pdf (101=1 term, 102=2 terms ...) 
1.00100e+00   1.00100e+00                          :: arbitrary series (model>100) omegaj (101=1 term, 102=2 terms ...) 
F 7.0  12.0 700 lineart-input.dat         :: calcT?, min log(t), max log(t), # times, time input filename 
1.0E-13 1.0E-14 50                 :: de Hoog et al. params: alpha, tolerance, M
variableP.out                         :: output filename
"""

input_string_dual="""F  1.0  T  1000.0  0.0  0                    :: specQ?, rD, inf?, xf, H, debug
%.5g  %.5g  %.5g  1.0 0.0 0.0        :: kappa, omegaf, yeD, alpha, Rf, Rm
101                                :: model (e.g, 1=lognormal, 10=slab diffusion, 101=dual porosity)
11 -1.5  2.0                     :: continuous model params (model=1): quadrature order, par(1:4)
10                               :: slab diffusion (model=10): N (~inf series)
%.5g    kappa/(1-omega)      :: arbitrary series (model>100) abcissa (101=1 term, 102=2 terms ...)
%.5g    1.0                   :: arbitrary series (model>100) pdf (101=1 term, 102=2 terms ...) 
%.5g    (1-omega)             :: arbitrary series (model>100) omegaj (101=1 term, 102=2 terms ...) 
F 7.0  12.0 700 lineart-input.dat         :: calcT?, min log(t), max log(t), # times, time input filename 
1.0E-13 1.0E-14 50                 :: de Hoog et al. params: alpha, tolerance, M
variableP.out                         :: output filename
"""

kapvec =  [1.E-4, 1.0E-6, 1.0E-8]
colors = ['red',  'green', 'black','orange','purple','cyan','magenta']

##omega = 1.0E-8
omegavec =  [0.01, 1.0E-4, 1.0E-6]
##LDvec = [10.0, 31.6, 100.0, 316.2, 1000.0]
LD = 1000.0
linetypes = [[1],[11,2],[7,2],[3,2],[1,1]]

# tD and relative pseudo pressure for variable pbh case
tvec = np.array([0.0, 5.516E+6, 1.000E+9, 2.0E+9, 1.0E+10, 2.4E+10]) 
pvec = np.array([0.0, 0.7,     0.275,      0.13,  0.005,  0.001]) 

last_line = str(stepmult*(len(tvec)-1)) +' '+ ' '.join(['%.5g' for x in range(2*len(tvec)-1)])+'\n'

if use_dual:
    input_string = input_string_dual + last_line
else:
    input_string = input_string_slab + last_line

exe = './multiporosity'

fig = plt.figure(1,figsize=(10,5))
ObsPax = fig.add_subplot(121)
ModQax = fig.add_subplot(122)
                
Tc = 0.001566461
DAYS2SEC = 24.0*60.0*60.0

datafn = './W2-export-bhp.csv'
d = np.loadtxt(datafn,delimiter=',',converters={0:mpd.datestr2num})

# data columns in file (columns 0-5 are original data; 
# 6-11 are computed via compute-bottomhole-pressure.py)
#
# 0  : date  (MM/DD/YY)
# 1  : Q_gas 
# 2  : Q_oil
# 3  : Q_water
# 4  : P_tubing
# 5  : P_casing
# 6  : est. bottomhole pressure (O&J)
# 7  : est. pseudopressure 
# 8  : est. downhole flowrate (Q*B)
# 9  : B
# 10 : est. bottomhole pressure (O&J)
# 11 : m/Q (pseudopressure/bottomhole flowrate)
# 12 : t_a (compressibility-only pseudotime)
# 13 : t_ca (compressibility and material balance pseudotime)

d[:,12] *= DAYS2SEC/Tc

m0 = 10000

ObsPax.plot(d[:,12],m0-d[:,7],'r.') # plot non-differenced pseudopressure
ObsPax.plot(tvec,pvec*m0,'k-')

ModQax.plot(d[:,12],d[:,8],'b.')
ModQax2 = ModQax.twinx()

for (kap,color),(omega,linetype) in product(zip(kapvec,colors),zip(omegavec,linetypes)):
#for (kap,color),(LD,linetype) in product(zip(kapvec,colors),zip(LDvec,linetypes)):

    print kap,LD,omega
    fh = open('multiporosity.in','w')
    if use_dual:
        args = [kap,omega,LD,kap/(1-omega),1.0,1-omega]
    else:
        args = [kap,omega,LD]
    args.extend(tvec)
    args.extend(pvec[:-1])
    
    fh.write(input_string % tuple(args))
    fh.close()
    call(exe)

    # specified Q (results are dimensionless press)
    t,z,dz = np.loadtxt('variableP.out',skiprows=11,unpack=True)

#    line = ModQax2.plot(t,z,color=color,linestyle='-',linewidth=1.5,
#                       marker='None',label='$\\kappa=%.2g$ $L_D=%.2g$' % (kap,LD))[0]
    line = ModQax2.plot(t,z,color=color,linestyle='-',linewidth=1.5,
                        marker='None',label='$\\kappa=%.2g$ $\\omega=%.2g$' % (kap,omega))[0]

    if len(linetype) > 1:
        line.set_dashes(linetype)

ModQax2.legend(loc=0,prop={'size':6},handlelength=3)
 
ObsPax.set_xlabel('$t_a$ & $t_D$')
ModQax.set_xlabel('$t_a$ & $t_D$')

ObsPax.set_xlim([0,2.4E+10])
ModQax.set_xlim([0,2.4E+10])
#ModQax2.set_ylim([-0.02,0.23])

ObsPax.set_ylabel('Pseudopressure [psi]')
ModQax.set_ylabel('Bottomhole flowrate $QB$')

plt.tight_layout()
plt.savefig('multiporosity-variable-bhp.pdf')
plt.close(1)
