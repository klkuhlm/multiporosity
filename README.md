This repository contains the modern Fortran source code for an
implementation of the multiporosity flow model; a generalization of
dual-porosity flow in low-permeability fractured rocks. (see "code"
directory).

*The input parameters are explained in input-explanation.txt*

The program performs numerical Laplace transform inversion for the
radially symmetric multiporosity flow solution (using modified Bessel
functions).  The program is free software (MIT license), which can
essentially be used, modified, or redistributed for any purpose, given
the license and acknowledgement of my authorship is left intact.

The repository also contains LaTeX source files, and figures used in
the paper published in Water Resources Research on the same topic
(main repository directory).

Kuhlman, K.L., B. Malama, & J.E. Heath, 2015. Multiporosity Flow in
Fractured Low-Permeability Rocks, Water Resources Research,
51(2):848-860.  http://dx.doi.org/10.1002/2014WR016502

The draft manuscript can also be downloaded from the arXiv manuscript
repository.  http://arxiv.org/abs/1502.03067

The source code is distributed as a collection of Fortran90/03 source
files and a makefile. It can be compiled for unix/linux/mac with
gfotran >= 4.6 installed in a typical location by just running

make

or

make driver

(these are equivalent).

You can compile a debugging version, with a few more checks and fewer
optimizations using:

make debug_driver

On MS-Windows, you will need the mingw compilation environment (OpenMP
doesn't work under mingw, though -- so single thread only) or the
Intel Fortran compiler (which works and provides OpenMP as well).  I
have compiled my tools with the free mingw toolchain, and can either
provide assistance setting this up, or provide you with a
binary. There are a few settings in the makefile that must be changed
to get it to compile using mingw (see comments there).

2024 Update
-----------
A paper has been released that is an update to the approach
given in this paper, and that paper also has a github.com page.

Kuhlman, K.L., 2024. Generalized solution for double-porosity flow through 
a graded excavation damaged zone, Mathematical Geosciences. 
https://doi.org/10.1007/s11004-024-10143-8

https://github.com/klkuhlm/graded
